$(function () {


  // credit BAR
  let C_amountBar = document.getElementById('C_amountBar');
  let C_amountInput = document.getElementById('C_amountInput');
  noUiSlider.create(C_amountBar, {
    start: [50],
    tooltips: true,
    step: 10,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': 10,
      'max': 500
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });
  C_amountBar.noUiSlider.on('update', function (values, handle) {
    C_amountInput.value = values[handle];
  });

  C_amountInput.addEventListener('change', function () {
    C_amountBar.noUiSlider.set(this.value);
  });

  let C_yearBar = document.getElementById('C_yearBar');
  let C_yearInput = document.getElementById('C_yearInput');
  noUiSlider.create(C_yearBar, {
    start: [7],
    tooltips: true,
    step: 1,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': 1,
      'max': 7
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });
  C_yearBar.noUiSlider.on('update', function (values, handle) {
    C_yearInput.value = values[handle];
  });

  C_yearInput.addEventListener('change', function () {
    C_yearBar.noUiSlider.set(this.value);
  });

  //房貸試算
  $(".loan_btn").click(function () {
    var loan_inner = $(".loan_inner").css('display')
    console.log(loan_inner);
    if (loan_inner == 'block') {
      $(".loan_inner").css("display", "none");
      $(".loan_btn").html('');
      $(".loan_btn").html('<a href="#">房貸試算</a><img class="arrow open" src=" / assets / images / icon / arrow - left_white.svg"  alt = "" >');
    } else {
      $(".loan_inner").css("display", "block");
      $(".loan_btn").html('');
      //這邊你自己去改打開之後的說明
      $(".loan_btn").html('<a href="#">以年利率3.56%方案計算</a><img class="arrow open" src=" / assets / images / icon / arrow - left_white.svg"  alt = "" >');
    }

  });



});
