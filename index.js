$(document).ready(function () {

  //數字千分位方法
  function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
  }
  //取消字串中出現的所有逗號
  function clear(str) {
    str = str.replace(/,/g, "");
    return str;
  }
  //-------------------------------------------------房貸餘額
  //初始化bar的設定


  var house_Mbar = document.getElementById('house_Mbar');
  noUiSlider.create(house_Mbar, {
    start: [800],
    behaviour: 'tap',
    tooltips: true,
    step: 10,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': 100,
      'max': 3000
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //新增按鈕
  var stepforward = document.getElementById('stepforward')

  var stepback = document.getElementById('stepback')

  stepforward.addEventListener('click', function () {
    var currentPosition = parseInt(house_Mbar.noUiSlider.get());
    var stepSize = 50;
    currentPosition += stepSize;
    house_Mbar.noUiSlider.set(currentPosition);
  });

  stepback.addEventListener('click', function () {
    var currentPosition = parseInt(house_Mbar.noUiSlider.get());
    var stepSize = 50;
    currentPosition -= stepSize;
    house_Mbar.noUiSlider.set(currentPosition);
  });







  //設定變數給input欄位
  var house_money = document.getElementById('house_money');

  //利用套件方法判斷bar的數字跟input數字同步
  house_Mbar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    house_money.value = FormatNumber(value);
  });

  house_money.addEventListener('change', function () {
    house_Mbar.noUiSlider.set(this.value);
  });






  var house_Ybar = document.getElementById('house_Ybar');
  noUiSlider.create(house_Ybar, {
    start: [15],
    behaviour: 'tap',
    tooltips: true,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': [1],
      'max': [30]
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //設定變數給input欄位
  var house_year = document.getElementById('house_year');

  //利用套件方法判斷bar的數字跟input數字同步
  house_Ybar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    house_year.value = value;
  });


  house_year.addEventListener('change', function () {
    house_Ybar.noUiSlider.set([this.value, null]);
  });


  //動態計算總額
  house_Mbar.noUiSlider.on('update', function (B, b) {

    house_Ybar.noUiSlider.on('update', function (C, c) {
      var B_MONEY = B[b];
      var C_YEAR = C[c];
      var rate = (1.31 * 0.01) / 12;
      var month = C_YEAR * 12;
      var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

      $("#house_month").html(FormatNumber(Math.round(A_ALL)));

      // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
    });
  });

  // ----------------------------------------------
  //信貸
  var credit_Mbar = document.getElementById('credit_Mbar');
  noUiSlider.create(credit_Mbar, {
    start: [85],
    behaviour: 'tap',
    tooltips: true,
    // step: 5,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': 10,
      'max': 500
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //設定變數給input欄位
  var credit_money = document.getElementById('credit_money');


  //利用套件方法判斷bar的數字跟input數字同步
  credit_Mbar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    credit_money.value = FormatNumber(value);

  });


  credit_money.addEventListener('change', function () {
    credit_Mbar.noUiSlider.set([this.value, null]);
  });


  var credit_Ybar = document.getElementById('credit_Ybar');
  noUiSlider.create(credit_Ybar, {
    start: [7],
    behaviour: 'tap',
    tooltips: true,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': [1],
      'max': [7]
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //設定變數給input欄位
  var credit_year = document.getElementById('credit_year');

  //利用套件方法判斷bar的數字跟input數字同步
  credit_Ybar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    credit_year.value = value;
  });


  credit_year.addEventListener('change', function () {
    credit_Ybar.noUiSlider.set([this.value, null]);
  });


  //動態計算總額
  credit_Mbar.noUiSlider.on('update', function (B, b) {

    credit_Ybar.noUiSlider.on('update', function (C, c) {
      var B_MONEY = B[b];
      var C_YEAR = C[c];
      var rate = (2.6 * 0.01) / 12;
      var month = C_YEAR * 12;
      var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

      $("#credit_month").html(FormatNumber(Math.round(A_ALL)));

      // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
    });
  });
  // ----------------------------------------------
  //車貸
  var car_Mbar = document.getElementById('car_Mbar');
  noUiSlider.create(car_Mbar, {
    start: [100],
    behaviour: 'tap',
    tooltips: true,
    // step: 10,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': 10,
      'max': 800
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //設定變數給input欄位
  var car_money = document.getElementById('car_money');


  //利用套件方法判斷bar的數字跟input數字同步
  car_Mbar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    car_money.value = FormatNumber(value);

  });


  car_money.addEventListener('change', function () {
    car_Mbar.noUiSlider.set([this.value, null]);
  });


  var car_Ybar = document.getElementById('car_Ybar');
  noUiSlider.create(car_Ybar, {
    start: [36],
    behaviour: 'tap',
    tooltips: true,
    format: {
      from: Number,
      to: function (value) {
        return (Math.round(value));
      }
    },
    range: {
      'min': [12],
      'max': [60]
    },
    pips: {
      mode: 'range',
      density: 100
    }
  });


  //設定變數給input欄位
  var car_year = document.getElementById('car_year');

  //利用套件方法判斷bar的數字跟input數字同步
  car_Ybar.noUiSlider.on('update', function (values, handle) {
    var value = values[handle];
    car_year.value = value;
  });


  car_year.addEventListener('change', function () {
    car_Ybar.noUiSlider.set([this.value, null]);
  });


  //動態計算總額
  car_Mbar.noUiSlider.on('update', function (B, b) {

    car_Ybar.noUiSlider.on('update', function (C, c) {
      var B_MONEY = B[b]; //車貸金額
      var C_Month = C[c]; //貸款期間
      var rate = (2.38 * 0.01) / 12; //利率÷12

      var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (C_Month)) * (rate)) / (Math.pow((1 + (rate)), (C_Month)) - 1)
      $("#car_month").html(FormatNumber(Math.round(A_ALL)));

      // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
    });
  });


});