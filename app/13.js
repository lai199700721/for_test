$(function () {
  var init = function () {
    CarBar_init();
    // var swiper = new Swiper('.swiper-container', {
    //   slidesPerView: 1,
    //   centeredSlides: true,
    //   pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    //   },
    // });
  };

  $('a.f_gold').on('click', function () {
    $('.lb_wrapper').addClass('show');
    $('body').on('click', '.lb_wrapper a.f_gold , .lb_wrapper a.lb_btn', function () {
      $('.lb_wrapper').removeClass('show');
    });
  });

  $('.loan_btn').on('click', function () {
    let InrateVal = $(this).siblings('.loan_item').find('.Inrate').text();
    console.log(InrateVal)
    $(this).toggleClass('loan_btn--action').siblings('.loan_item').children('.loan_inner').slideToggle();
    $(this).siblings('.loan_item').children('.payment-msg').toggleClass('hide');
    $(this).siblings('.loan_item').children('.loan_title').find('a').toggle();

    let Action = $('.loan_btn').hasClass('loan_btn--action');
    if (Action === true) {
      $(this).find('a').html(`以年利率${InrateVal}%方案計算`);
    } else {
      $(this).find('a').html(`貸款式算`);
    }
  });

  function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
  };

  function AddAni(id) {
    id.noUiSlider.on('slide', function () {
      let animated = $('.animated-item');
      console.log(animated)
      let hasanimated = animated.hasClass('ani');
      console.log(hasanimated);
      if (!hasanimated) {
        animated.addClass("ani");
        return;
      } else {
        animated.removeClass("ani");
        return;
      }
    })
  }

  function CarBar_init() {
    let Car_amountBar = document.getElementById('Car_amountBar');
    let Car_amountInput = document.getElementById('Car_amountInput');
    noUiSlider.create(Car_amountBar, {
      start: [100],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 10,
        'max': 300
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
    // Connecting the input field
    Car_amountBar.noUiSlider.on('update', function (values, handle) {
      Car_amountInput.value = values[handle];
      AddAni(Car_amountBar);
      // get value change images
      let self = this;
      let Credit_amounVal = self.get();
      let animated_item = document.querySelector('.animated-item');

      if (Credit_amounVal <= 80) {
        animated_item.src = "/app/images/car1.svg";
      } else if (Credit_amounVal >= 81 && Credit_amounVal <= 120) {
        animated_item.src = "/app/images/car2.svg";
      } else if (Credit_amounVal >= 121) {
        animated_item.src = "/app/images/car3.svg";
      };


      let elements = document.querySelectorAll('.noUi-tooltip');


      if (elements.length) {
        elements[0].id = 'Car_amountBar_tooltip';
      }

      let Car_amountBar_tooltip = $('#Car_amountBar_tooltip').html();
      console.log(`tooltip:${Car_amountBar_tooltip}`);

      //拉霸位置判斷式
      if (Car_amountBar_tooltip >= 285) {
        $('#Car_amountBar_amountBar_tooltip').css({
          transform: "translate(-92%, 50%)",
        });
      } else if (Car_amountBar_tooltip <= 30) {
        $('#Car_amountBar_tooltip').css({
          transform: "translate(-8%, 50%)",
        });
      } else {
        $('#Car_amountBar_tooltip').css({
          transform: "translate(-50%, 50%)",
        });
      };
    });

    Car_amountInput.addEventListener('change', function () {
      Car_amountBar.noUiSlider.set(this.value);
    });

    let Car_yearBar = document.getElementById('Car_yearBar');
    let Car_yearInput = document.getElementById('Car_yearInput');
    noUiSlider.create(Car_yearBar, {
      start: [5],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 5
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    Car_yearBar.noUiSlider.on('update', function (values, handle) {
      Car_yearInput.value = values[handle];
      var elements = document.querySelectorAll('.noUi-tooltip');
      if (elements.length) {
        elements[1].id = 'Car_yearBar_tooltip';
      }
      var Car_yearBar_tooltip = $('#Car_yearBar_tooltip').html();
      console.log(Car_yearBar_tooltip);

      //拉霸位置判斷式
      if (Car_yearBar_tooltip >= 5) {
        $('#Car_yearBar_tooltip').css({
          transform: "translate(-92%, 50%)",
        });
      } else if (Car_yearBar_tooltip <= 1) {
        $('#Car_yearBar_tooltip').css({
          transform: "translate(-8%, 50%)",
        });
      } else {
        $('#Car_yearBar_tooltip').css({
          transform: "translate(-50%, 50%)",
        });
      };
    });

    Car_yearInput.addEventListener('change', function () {
      Car_yearBar.noUiSlider.set(this.value);
    });
    //動態計算總額
    Car_amountBar.noUiSlider.on('update', function (B, b) {

      Car_yearBar.noUiSlider.on('update', function (C, c) {
        var B_MONEY = B[b]; //車貸金額
        var C_Month = C[c]; //貸款期間
        var rate = (2.38 * 0.01) / 12; //利率÷12

        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (C_Month * 12)) * (rate)) / (Math.pow((1 + (rate)), (C_Month * 12)) - 1)


        $("#CarPayableAmount").html(FormatNumber(Math.round(A_ALL)));
        $(".LoanAmount").html(B_MONEY);
        $(".LoanPeriod").html(C_Month);
        $(".MonthlyPayment").html(FormatNumber(Math.round(A_ALL)));

        // console.log(`B_MONEY:${B_MONEY},C_Month:${C_Month},rate:${rate},month:${ A_ALL}`)
      });
    });
  };

  init();
});
