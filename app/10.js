$(function () {
  var init = function () {
    CreditBar_init();
    // var swiper = new Swiper('.swiper-container', {
    //   slidesPerView: 1,
    //   centeredSlides: true,
    //   pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    //   },
    // });
  };

  $('a.f_gold').on('click', function () {
    $('.lb_wrapper').addClass('show');
    $('body').on('click', '.lb_wrapper a.f_gold , .lb_wrapper a.lb_btn', function () {
      $('.lb_wrapper').removeClass('show');
    });
  });

  $('.loan_btn').on('click', function () {
    let InrateVal = $(this).siblings('.loan_item').find('.Inrate').text();
    console.log(InrateVal)
    $(this).toggleClass('loan_btn--action').siblings('.loan_item').children('.loan_inner').slideToggle();
    $(this).siblings('.loan_item').children('.payment-msg').toggleClass('hide');
    $(this).siblings('.loan_item').children('.loan_title').find('a').toggle();

    let Action = $('.loan_btn').hasClass('loan_btn--action');
    if (Action === true) {
      $(this).find('a').html(`以年利率${InrateVal}%方案計算`);
    } else {
      $(this).find('a').html(`貸款式算`);
    }
  });

  function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
  };

  // function AddAni(id) {
  //   id.noUiSlider.on('slide', function () {
  //     let animated = $('.animated-item');
  //     console.log(animated)
  //     let hasanimated = animated.hasClass('ani');
  //     console.log(hasanimated);
  //     if (!hasanimated) {
  //       animated.addClass("ani");
  //       return;
  //     } else {
  //       animated.removeClass("ani");
  //       return;
  //     }
  //   })
  // }


  function CreditBar_init() {
    let Credit_amountBar = document.getElementById('Credit_amountBar');
    let Credit_amountInput = document.getElementById('Credit_amountInput');
    noUiSlider.create(Credit_amountBar, {
      start: [50],
      tooltips: true,
      connect: [true, false],
      step: 10,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 10,
        'max': 500
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
    // Connecting the input field
    // Credit_amountBar.noUiSlider.on('update', function (values, handle) {
    //   Credit_amountInput.value = values[handle];
    //   // AddAni(Credit_amountBar);
    //   // get value change images
    // });

    Credit_amountInput.addEventListener('change', function () {
      Credit_amountBar.noUiSlider.set(this.value);
    });

    let Credit_yearBar = document.getElementById('Credit_yearBar');
    let Credit_yearInput = document.getElementById('Credit_yearInput');
    noUiSlider.create(Credit_yearBar, {
      start: [7],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 7
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    // Credit_yearBar.noUiSlider.on('update', function (values, handle) {
    //   Credit_yearInput.value = values[handle];
    // });

    Credit_yearInput.addEventListener('change', function () {
      Credit_yearBar.noUiSlider.set(this.value);
    });
    //計算總額

    //起始值
    var right = "50>60";
    var left = "50>40";

    Credit_amountBar.noUiSlider.on('update', function (B, b) {
      Credit_amountInput.value = B[b];
      let self = this;
      let Credit_amounVal = self.get();
      let animated_item = document.querySelector('.animated-item');
      let animated = $('.animated-item');



      //固定在某一個時點換圖片 也設定在某一個時點加class
      if (Credit_amounVal < 30) {
        animated_item.src = "/app/images/money1.svg";


      } else if (Credit_amounVal >= 30 && Credit_amounVal < 60) {

        animated_item.src = "/app/images/money2.svg";


      } else if (Credit_amounVal > 50) {
        animated_item.src = "/app/images/money3.svg";

      } 

      // 從60過去的70
      if (Credit_amounVal >= 70) {
        animated.removeClass("ani");
        right = "50>60";
        left = "70>60";
      }


      //從70過去的60 從50過去的60 
      if ((Credit_amounVal == 60 && left == "70>60") || (Credit_amounVal == 60 && right == "50>60")) {
        if (left == "70>60") {
          animated.removeClass("ani");
        } else if (right == "50>60") {
          animated.removeClass("ani");
          setTimeout(function () {
            animated.addClass("ani");
          }, 500);
        }
        right = "60>70";
        left = "60>50";
      }

      //起始先往右 從50過去的60 
      // if (Credit_amounVal == 60 && right == "50>60") {
      //   animated.addClass("ani");
      //   right = "60>70";
      //   left = "60>50";
      // }

      //從60過去的50 從40過去的50
      if ((Credit_amounVal == 50 && left == "60>50") || (Credit_amounVal == 50 && right == "40>50")) {
        if (left == "60>50") {
          animated.removeClass("ani");
          setTimeout(function () {
            animated.addClass("ani");
          }, 500);
        } else if (right == "40>50") {
          animated.removeClass("ani");
        }
        right = "50>60";
        left = "50>40";
      }

      //從40過去的50
      // if (Credit_amounVal == 50 && right == "40>50") {
      //   animated.removeClass("ani");
      //   right = "50>60";
      //   left = "50>40";
      // }

      //從50過去的40 || 從30過去的40
      if ((Credit_amounVal == 40 && left == "50>40") || (Credit_amounVal == 40 && right == "30>40")) {
        animated.removeClass("ani");
        right = "40>50";
        left = "40>30";
      }

      //從30過去的40
      // if (Credit_amounVal == 40 && right == "30>40") {
      //   animated.removeClass("ani");
      //   right = "40>50";
      //   left = "40>30";
      // }

      //從40過去的30 從20過去的30
      if ((Credit_amounVal == 30 && left == "40>30") || (Credit_amounVal == 30 && right == "20>30")) {
        if (left == "40>30") {
          animated.removeClass("ani");
        } else if (right == "20>30") {
          animated.removeClass("ani");
          setTimeout(function () {
            animated.addClass("ani");
          }, 500);
        }
        right = "30>40";
        left = "30>20";
      }

      //從20過去的30
      // if (Credit_amounVal == 30 && right == "20>30") {
      //   animated.addClass("ani");
      //   right = "30>40";
      //   left = "30>20";
      // }

      //從30過去的20 從10過去的20
      if ((Credit_amounVal == 20 && left == "30>20") || (Credit_amounVal == 20 && right == "10>20")) {
        if (left == "30>20") {
          animated.removeClass("ani");
          setTimeout(function () {
            animated.addClass("ani");
          }, 500);
        } else if (right == "10>20") {
          animated.removeClass("ani");
        }
        right = "20>30";
        left = "20>10";
      }

      //從10過去的20
      // if (Credit_amounVal == 20 && right == "10>20") {
      //   animated.removeClass("ani");
      //   right = "20>30";
      //   left = "20>10";
      // }

      //從20過去的10
      if (Credit_amounVal == 10 && left == "20>10") {
        animated.removeClass("ani");
        right = "10>20";
      }


      // tooltip postion
      let elements = document.querySelectorAll('.noUi-tooltip');
      if (elements.length) {
        elements[0].id = 'Credit_amountBar_tooltip';
      }
      let Credit_amountBar_tooltip = $('#Credit_amountBar_tooltip').html();
      console.log(`tooltip:${Credit_amountBar_tooltip}`);
      //拉霸位置判斷式
      if (Credit_amountBar_tooltip >= 470) {
        $('#Credit_amountBar_tooltip').css({
          transform: "translate(-92%, 50%)",
        });
      } else if (Credit_amountBar_tooltip <= 30) {
        $('#Credit_amountBar_tooltip').css({
          transform: "translate(-8%, 50%)",
        });
      } else {
        $('#Credit_amountBar_tooltip').css({
          transform: "translate(-50%, 50%)",
        });
      };


      Credit_yearBar.noUiSlider.on('update', function (C, c) {

        Credit_yearInput.value = C[c];

        var elements = document.querySelectorAll('.noUi-tooltip');
        if (elements.length) {
          elements[1].id = 'Credit_yearBar_tooltip';
        }
        var Credit_yearBar_tooltip = $('#Credit_yearBar_tooltip').html();
        console.log(`yearBar:${Credit_yearBar_tooltip}`);

        //拉霸位置判斷式
        if (Credit_yearBar_tooltip >= 7) {
          $('#Credit_yearBar_tooltip').css({
            transform: "translate(-92%, 50%)",
          });
        } else if (Credit_yearBar_tooltip <= 1) {
          $('#Credit_yearBar_tooltip').css({
            transform: "translate(-8%, 50%)",
          });
        } else {
          $('#Credit_yearBar_tooltip').css({
            transform: "translate(-50%, 50%)",
          });
        };

        var B_MONEY = B[b];
        var C_YEAR = C[c];
        var rate = (2.6 * 0.01) / 12;
        var month = C_YEAR * 12;
        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

        $("#CreditPayableAmount").html(FormatNumber(Math.round(A_ALL)));

        $(".LoanAmount").html(B_MONEY);
        $(".LoanPeriod").html(C_YEAR);
        $(".MonthlyPayment").html(FormatNumber(Math.round(A_ALL)));

        // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
      });
    });
  };

  init();
});
