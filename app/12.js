$(function () {
  var init = function () {
    MortgageBar_init();
    // var swiper = new Swiper('.swiper-container', {
    //   slidesPerView: 1,
    //   centeredSlides: true,
    //   pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    //   },
    // });
  };

  $('a.f_gold').on('click', function () {
    $('.lb_wrapper').addClass('show');
    $('body').on('click', '.lb_wrapper a.f_gold , .lb_wrapper a.lb_btn', function () {
      $('.lb_wrapper').removeClass('show');
    });
  });

  $('.loan_btn').on('click', function () {
    let InrateVal = $(this).siblings('.loan_item').find('.Inrate').text();
    console.log(InrateVal)
    $(this).toggleClass('loan_btn--action').siblings('.loan_item').children('.loan_inner').slideToggle();
    $(this).siblings('.loan_item').children('.payment-msg').toggleClass('hide');
    $(this).siblings('.loan_item').children('.loan_title').find('a').toggle();

    let Action = $('.loan_btn').hasClass('loan_btn--action');
    if (Action === true) {
      $(this).find('a').html(`以年利率${InrateVal}%方案計算`);
    } else {
      $(this).find('a').html(`貸款式算`);
    }
  });

  function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
  };

  function AddAni(id) {
    id.noUiSlider.on('slide', function () {
      let animated = $('.animated-item');
      console.log(animated)
      let hasanimated = animated.hasClass('ani');
      console.log(hasanimated);
      if (!hasanimated) {
        animated.addClass("ani");
        return;
      } else {
        animated.removeClass("ani");
        return;
      }
    })
  }

  function MortgageBar_init() {
    let M_amountBar = document.getElementById('M_amountBar');
    let M_amountInput = document.getElementById('M_amountInput');
    noUiSlider.create(M_amountBar, {
      start: [800],
      tooltips: true,
      connect: [true, false],
      step: 10,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 100,
        'max': 3000
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
    // Connecting the input field
    M_amountBar.noUiSlider.on('update', function (values, handle) {
      M_amountInput.value = values[handle];
      AddAni(M_amountBar);
      // get value change images
      let self = this;
      let Credit_amounVal = self.get();
      let animated_item = document.querySelector('.animated-item');

      if (Credit_amounVal <= 800) {
        animated_item.src = "/app/images/house1.svg";

      } else if (Credit_amounVal >= 801 && Credit_amounVal <= 1500) {
        animated_item.src = "/app/images/house2.svg";

      } else if (Credit_amounVal >= 1501) {
        animated_item.src = "/app/images/house3.svg";
      };


      let elements = document.querySelectorAll('.noUi-tooltip');
      if (elements.length) {
        elements[0].id = 'M_amountBar_tooltip';
      }
      let M_amountBar_tooltip = $('#M_amountBar_tooltip').html();
      console.log(`tooltip:${M_amountBar_tooltip}`);

      //拉霸位置判斷式
      if (M_amountBar_tooltip >= 2790) {
        $('#M_amountBar_amountBar_tooltip').css({
          transform: "translate(-92%, 50%)",
        });
      } else if (M_amountBar_tooltip <= 350) {
        $('#M_amountBar_amountBar_tooltip').css({
          transform: "translate(-8%, 50%)",
        });
      } else {
        $('#M_amountBar_tooltip').css({
          transform: "translate(-50%, 50%)",
        });
      };

    });

    M_amountInput.addEventListener('change', function () {
      M_amountBar.noUiSlider.set(this.value);
    });

    let M_yearBar = document.getElementById('M_yearBar');
    let M_yearInput = document.getElementById('M_yearInput');
    noUiSlider.create(M_yearBar, {
      start: [30],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 40
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    M_yearBar.noUiSlider.on('update', function (values, handle) {
      M_yearInput.value = values[handle];

      var elements = document.querySelectorAll('.noUi-tooltip');
      if (elements.length) {
        elements[1].id = 'M_yearBar_tooltip';
      }
      var M_yearBar_tooltip = $('#M_yearBar_tooltip').html();
      console.log(M_yearBar_tooltip);

      //拉霸位置判斷式
      if (M_yearBar_tooltip >= 38) {
        $('#M_yearBar_tooltip').css({
          transform: "translate(-92%, 50%)",
        });
      } else if (M_yearBar_tooltip <= 4) {
        $('#M_yearBar_tooltip').css({
          transform: "translate(-8%, 50%)",
        });
      } else {
        $('#M_yearBar_tooltip').css({
          transform: "translate(-50%, 50%)",
        });
      };
    });

    M_yearInput.addEventListener('change', function () {
      M_yearBar.noUiSlider.set(this.value);
    });
    //動態計算總額
    M_amountBar.noUiSlider.on('update', function (B, b) {

      M_yearBar.noUiSlider.on('update', function (C, c) {
        var B_MONEY = B[b];
        var C_YEAR = C[c];
        var rate = (1.31 * 0.01) / 12;
        var month = C_YEAR * 12;
        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

        $("#HousePayableAmount").html(FormatNumber(Math.round(A_ALL)));
        $(".LoanAmount").html(B_MONEY);
        $(".LoanPeriod").html(C_YEAR);
        $(".MonthlyPayment").html(FormatNumber(Math.round(A_ALL)));
        // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
      });
    });
  };


  init();
});
