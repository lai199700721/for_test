<
!DOCTYPE html >
  <
  html lang = "en" >

  <
  head >
  <
  meta http - equiv = "Content-Type"
content = "text/html; charset=utf-8" / >
  <
  meta http - equiv = "X-UA-Compatible"
content = "IE=Edge,chrome=1" / >
  <
  meta name = "viewport"
content = "width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no , minimal-ui" / >
  <
  meta name = "apple-mobile-web-app-capable"
content = "yes" >
  <
  meta name = "mobile-web-app-capable"
content = "yes" >
  <
  meta name = "format-detection"
content = "telephone=no" / >
  <
  meta name = "SKYPE_TOOLBAR"
content = "SKYPE_TOOLBAR_PARSER_COMPATIBLE" / >
  <
  !-- < meta property = "og:title"
content = "Richart - 最能幫年輕人存到錢的銀行" / > -- >
  <
  meta property = "og:description"
name = "description"
content = "最多人使用的數位銀行，最高 1%高利活儲" / >
  <
  meta property = "og:type"
content = "website" / >
  <
  !--og: url 每一頁指定自己的網址 EX: https: //richart.tw/TSDIB_RichartWeb/RC00/RC000000/ -->
  <
  meta property = "og:url"
content = "" / >
  <
  !--og: image 請指定完整網址 EX: https: //richart.tw/TSDIB_RichartWeb/RC00/IMAGE/FB.jpg -->

  <
  meta property = "og:image"
content = "https://richart.tw/TSDIB_RichartWeb/static/img/common/preview.png" / >
  <
  meta property = "og:image:type"
content = "image/png" / >
  <
  meta property = "og:image:width"
content = "1200" / >
  <
  meta property = "og:image:height"
content = "630" / >
  <
  meta property = "og:site_name"
content = "Richart - 最能幫年輕人存到錢的銀行" / >
  <
  title > Richart - 最能幫年輕人存到錢的銀行 < /title>

  <
  !--20200217 Tom 增加更新靜態檔案版本號 有更新文件記得改-- >










  <
  !--舊版樣式區塊 start-- >
  <
  link rel = "shortcut icon"
type = "image/x-icon"
href = "/TSDIB_RichartWeb/static/img/common/favicon.ico?img=20170811_01" >




  <
  script async src = "https://www.googletagmanager.com/gtag/js?id=UA-72250086-1" > < /script> <
script type = "text/javascript" >

  var gaTrackId = 'UA-72250086-1';

//gtag use
window.dataLayer = window.dataLayer || [];

function gtag() {
  dataLayer.push(arguments);
}
gtag('js', new Date());




// function ga_send_user_data(b_no,c_no){
// 	if(b_no==''){
// 		b_no = "null";
// 	}
// 	if(c_no==''){
// 		c_no = "null";
// 	}
// 	console.warn("update dimension -> dimension4 = " + b_no + " , dimension7 = " + c_no );
// 	gtag('set', { 'dimension4': b_no, 'dimension7': c_no });
// }

function ga_send_user_data_b(b_no) {
  if (b_no == '') {
    b_no = "null";
  }
  console.warn("update dimension -> dimension4 = " + b_no);
  gtag('set', {
    'dimension4': b_no
  });
}

function ga_send_user_data_c(c_no) {
  if (c_no == '') {
    c_no = "null";
  }
  console.warn("update dimension -> dimension7 = " + c_no);
  gtag('set', {
    'dimension7': c_no
  });
}


function ga_send_registry_data(m_no) {
  if (m_no == '') {
    m_no = "null";
  }
  console.warn("update dimension -> dimension6 = " + m_no);
  gtag('set', {
    'dimension6': m_no
  });
}



function ga_send_parents_c_no(p_c_no) {
  if (p_c_no == '' || p_c_no == null) {
    p_c_no = "null";
  }
  console.warn("update dimension -> dimension10 = " + p_c_no);
  gtag('set', {
    'dimension10': p_c_no
  });
}

function ga_send_parents_b_no(p_b_no) {
  if (p_b_no == '' || p_b_no == null) {
    p_b_no = "null";
  }
  console.warn("update dimension -> dimension8 = " + p_b_no);
  gtag('set', {
    'dimension8': p_b_no
  });
}

function ga_send_parents_m_no(p_m_no) {
  if (p_m_no == '' || p_m_no == null) {
    p_m_no = "null";
  }
  console.warn("update dimension -> dimension9 = " + p_m_no);
  gtag('set', {
    'dimension9': p_m_no
  });
}



function getUtmTerm() {
  var nowpath = window.location.href;
  var utm_no = null;
  var queryObj = {};
  nowpath.replace(
    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    function ($0, $1, $2, $3) {
      queryObj[$1] = $3;
    }
  );
  if (queryObj['utm_term'] != undefined) {
    utm_no = queryObj['utm_term'];
  }
  return utm_no;
}


var getUtmEventStr = function () {
  var utmEventStr = "";
  $.getParamString = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  }

  if ($.getParamString('utm_source') && ($.getParamString('second_source') || $.getParamString('second_medium') || $.getParamString('second_campaign') || $.getParamString('second_content'))) {
    utmEventStr += '[' + ($.getParamString('utm_source') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('utm_medium') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('utm_campaign') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('utm_content') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('second_source') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('second_medium') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('second_campaign') || '無') + '],';
    utmEventStr += '[' + ($.getParamString('second_content') || '無') + ']';
  }

  return utmEventStr;
}

function ga_trace_page(title) {
  if (typeof (title) === 'undefined') {
    title = "";
  }
  traceView(title);
}

function ga_trace_event(catg, act, label) {
  traceEvent(catg, act, label);
}

function ga_trace_event_utm(eventTitle_prefix) {
  var utm_catg = 'V2_流量UTM來源內容';
  var utm_act = 'V2_流量UTM來源內容_1廣告來源媒介活動內容_2廣告來源媒介活動內容';
  var utm_label = eventTitle_prefix + ',';
  if (getUtmEventStr() != '') {
    utm_label += getUtmEventStr();
    traceEvent(utm_catg, utm_act, utm_label);
  }
}

// 2020-05-05 Sprint76 GA#26 Rachel
function ga_trace_event_click(e) {
  var id = e.id;
  var isExpanded = $('#' + id + ' a').attr('aria-expanded');
  // 點擊前題目尚未展開 
  if (isExpanded == 'false') {
    ga_trace_event($('#pageTitle').val(), $('#eventTitle').val(), 'true');
  }
}


// Sprint71 WEB#74 2020/01/10
// 子畫面事件
function ga_trace_sub_page(subTitle) {
  pageTitle = $("#pageTitle").val();
  ga_trace_page(pageTitle + "_" + subTitle);
}

//追蹤頁面名稱
var traceView = function (pageTitle) {
  var utm_no = null;

  utm_no = getUtmTerm();
  if (utm_no != null) {
    console.warn("gtag -> traceView 自訂維度 : user_id = " + utm_no + " , dimension1 = " + utm_no);
    gtag('set', {
      'user_id': utm_no,
      'dimension1': utm_no
    });
  }

  if (typeof (pageTitle) !== 'undefined' && pageTitle != '') {
    console.warn("gtag traceView -> pageTitle : " + pageTitle);
    gtag('config', gaTrackId, {
      'page_path': pageTitle,
      'custom_map': {
        'dimension2': 'client_id'
      }
    });
  } else {
    console.warn("gtag traceView");
    gtag('config', gaTrackId, {
      'custom_map': {
        'dimension2': 'client_id'
      }
    });
  }

};

//追蹤事件 類別,動作,事件標籤
var traceEvent = function (catg, act, label) {
  console.warn("gtag traceEvent -> category : " + catg + " , action : " + act + " , label : " + label);
  gtag('event', act, {
    'event_category': catg,
    'event_label': label
  })
}; <
/script> <!--舊小幫手樣式-- >



<
!--舊版樣式區塊 end-- >

<
!--新版樣式載入-- >
<
link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/NotoSansTC.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/bootstrap.min.css" >

  <
  script src = "/TSDIB_RichartWeb/static/revamp/js/bodyScrollLock.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jquery.min.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/popper.min.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/bootstrap.min.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/owl.carousel.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/aos.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jquery-ui.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jquery.ui.touch-punch.min.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/mobile-detect.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jquery.scrollbar.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/commonModule.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/keybord_s.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jquery.validate.min.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/plugin/jquery-blockUI/jquery.blockUI.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/jqueryValidationModules.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/popupModule.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/dragAreaModule.js?v=2020110501" > < /script>

  <
  script src = "/TSDIB_RichartWeb/static/revamp/js/spin.js" > < /script> <
link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/owl.carousel.min.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/owl.theme.default.min.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/aos.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/icon.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/jquery-ui.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/keybord_s.css?v=2020110501" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/main.css?v=2020110501" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/style.css?v=2020110501" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/jquery.scrollbar.css" >
  <
  link rel = "stylesheet"
href = "/TSDIB_RichartWeb/static/revamp/css/selfCss.css?v=2020110501" >





  <
  !--2021 - 4 - 23 Mantis #0016173 移除yahoo追蹤碼 by May -->



		



<!-- Facebook Pixel Code -->



<script> !function(f,b,e,v,n,t,s){



	if(f.fbq)return;n= f.fbq = function () {
    n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
  };
if (!f._fbq) f._fbq = n;
n.push = n;
n.loaded = !0;
n.version = '2.0';
n.queue = [];
t = b.createElement(e);
t.async = !0;
t.src = v;
s = b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t, s)
}
(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js'); <
!--2021 - 4 - 23 Mantis #0016172 新增代理商帳號(春樹) by May -->



			fbq('init', '942393739834619');



			



			<!-- 舊代理商(2008)  -->



			fbq('init', '159045918112637'); 



			fbq('track', 'PageView');



</script> 



<!--2021-4-23 Mantis# 0016172 新增代理商帳號(春樹) by May-- >
  <
  noscript > < img height = "1"
width = "1"
style = "display:none"
src = "https://www.facebook.com/tr? id=942393739834619&ev=PageView&noscript=1" / > < /noscript>

  <
  !--舊代理商(2008) -- >
  <
  noscript > < img height = "1"
width = "1"
style = "display:none"
src = "https://www.facebook.com/tr?id=159045918112637&ev=PageView&noscript=1" / > < /noscript> <!--End Facebook Pixel Code-- >

  <
  !--2021 - 4 - 23 Mantis #0016172 新增代理商帳號(春樹) by May -->



<!-- Global site tag (gtag.js) - Google Ads: 411735956 -->



<script async src= "https://www.googletagmanager.com/gtag/js?id=AW-411735956" > < /script> <!--舊代理商(2008) -- >
  <
  script async src = "https://www.googletagmanager.com/gtag/js?id=AW-736484076" > < /script> <
script >

  <
  !--Global site tag(gtag.js) - Google Ads: 736484076-- >
  window.dataLayer = window.dataLayer || [];

function gtag() {
  dataLayer.push(arguments);
}
gtag('js', new Date()); <
!--2021 - 4 - 23 Mantis #0016172 新增代理商帳號(春樹) by May -->



	  	gtag('config', 'AW-411735956');



	  	



	  	<!--舊代理商(2008) -->



	  	gtag('config', 'AW-736484076');



	  	gtag('config', 'AW-735907752');



	  	gtag('config', 'AW-735907806');



		gtag('config', 'AW-666938272');



		gtag('config', 'AW-696639033');



	</script>



	<!-- Event snippet for 到過Richart官網者 remarketing page -->



	<script>



	  gtag('event', 'conversion', {



	      'send_to': 'AW-736484076/IQhICOfcpaIBEOy1l98C',



	      'value': 1.0,



	      'currency': 'TWD',



	      'aw_remarketing_only': true



	  });



	</script>



	<!-- Event snippet for SEM_PC開戶按鈕 conversion page



	In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->



	<script>



	function SEM_report_conversion(url) {



	  var callback_SEM =

function () {
  if (typeof (url) != 'undefined') {
    window.location = url;
  }
};
gtag('event', 'conversion', {
  'send_to': 'AW-735907752/YoCUCLL1gNQBEKif9N4C',
  'event_callback': callback_SEM
});
return false;
} <
/script> <!--Event snippet
for GDN_PC開戶按鈕 conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button.-- >
  <
  script >
  function GDN_report_conversion(url) {
    var callback_GDN = function () {
      if (typeof (url) != 'undefined') {
        window.location = url;
      }
    };
    gtag('event', 'conversion', {
      'send_to': 'AW-735907806/GomDCJL88tMBEN6f9N4C',
      'event_callback': callback_GDN
    });
    return false;
  } <
  /script> <!--Event snippet
for 綠狗卡_PC開戶按鈕 conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button.-- >
  <
  script >
  function VD_report_conversion(url) {
    var callback_VD = function () {
      if (typeof (url) != 'undefined') {
        window.location = url;
      }
    };
    gtag('event', 'conversion', {
      'send_to': 'AW-666938272/79ePCLPcl9QBEKDXgr4C',
      'event_callback': callback_VD
    });
    return false;
  } <
  /script> <!--Event snippet
for AI_PC開戶按鈕 conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button.-- >
  <
  script >
  function AI_report_conversion(url) {
    var callback_AI = function () {
      if (typeof (url) != 'undefined') {
        window.location = url;
      }
    };
    gtag('event', 'conversion', {
      'send_to': 'AW-696639033/j8heCJHkl9QBELm8l8wC',
      'event_callback': callback_AI
    });
    return false;
  } <
  /script> <!--2021 - 4 - 23 Mantis #0016172 新增代理商事件功能(春樹) by May -->



	<!-- Event snippet for Richart 立即開戶 conversion page



	In your html page, add the snippet and call entry_report_conversion when someone clicks on the chosen link or button. -->



	<script>



	function entry_report_conversion(url) {



	  var callback =

function () {
  if (typeof (url) != 'undefined') {
    window.location = url;
  }
};
gtag('event', 'conversion', {
  'send_to': 'AW-411735956/RVbFCPfug_wBEJSvqsQB',
  'event_callback': callback
});
return false;
} <
/script>




<
title >
  Richart - 產品頁 - 車貸 <
  /title>




  <
  /head>

  <
  body id = "ng-app"
ng - app = "Richart-Ctrl"
ng - controller = "Ctrl" >
  <
  !--header 導覽列 Start-- >









  <
  header id = 'mainMenu'
class = 'menu menu-web' >
  <
  nav class = 'navbar navbar-expand-lg navbar-light bg-light menu-bg' >
  <
  div class = 'container' >

  <
  div class = 'menu-logo' > < a class = 'navbar-brand mr-auto'
href = "/TSDIB_RichartWeb/RC00/RC000000" > < img width = '156px'
height = '44px'
src = "/TSDIB_RichartWeb/static/revamp/images/img-logo.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/img-logo@2x.png 2x" / > < /a></div >
  <
  div class = 'form-inline menu-product' >
  <
  div id = 'prodList'
class = 'btn line-green-fix btn-product' >
  <
  div class = 'btn-area' > < i class = 'icons-ic-others-items mx-auto d-inline-block' > < /i><span class='px-2'>產品服務</span > < /div> <
div class = 'menu-p-triangle' > < /div> < /
  div > <
  /div>

  <
  div class = 'd-flex order-lg-last menu-burger' >
  <
  !--2021 - 03 - 31 WB #3 Roy 官網加入登入網銀按鈕 -->



				<!-- 2021-04-26 Mantis# 0016172 新代理商行銷碼事件埋設(春樹) by May-- >
  <
  a href = "/TSDIB_RichartWeb/RC03/RC030100"
class = 'btn btn-opening'
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊開戶&驗證按鈕','true'); entry_report_conversion('/TSDIB_RichartWeb/RC03/RC030100');" > 開戶 & 驗證 < /a>    <
button type = 'button'
data - toggle = 'collapse'
data - target = '#mobileNavbarToggler'
aria - controls = 'mobileNavbarToggler'
aria - expanded = 'false'
aria - label = 'Toggle navigation' >
  <
  div class = 'menu-hamburger' > < span > < /span><span></span > < span > < /span><span></span > < /div> < /
  button > <
  /div> <!--2020 - 05 - 06 Sprint76 GA #26 Rachel： menu add GA -->



            <div class= 'collapse navbar-collapse navbar-collapse-style justify-content-lg-end menu-head-item'
id = 'mobileNavbarToggler' >
  <
  ul id = 'prodMenu'
class = 'navbar-nav align-items-lg-center m-floor m-bomb' >
  <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-savemoney i-icon menu-icon' > < /i><span>儲蓄/外匯 < /span> <
i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  !--2020 - 08 - 20 mantis #13669 add -->



                                <div class= 'menu-item two-sort img-savemoney' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/DemandDeposit"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','儲蓄/外匯_台幣活存');" > 臺幣活存 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/ForeignCurrency"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','儲蓄/外匯_外幣活存');" > 外幣活存 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/TimeDeposit"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','儲蓄/外匯_定存');" > 定存 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/SubAccount"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','儲蓄/外匯_小查罐');" > 小查罐 < /i></a > < /li> <
li > < a href = "/TSDIB_RichartWeb/Products/VisaDebitCard"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','儲蓄/外匯_VISA金融卡');" > VISA金融卡 < /a></li >
  <
  li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> < /
  ul > <
  /div> < /
  div > <
  /div> < /
  li > <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-card i-icon menu-icon' > < /i><span>信用卡</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  div class = 'menu-item img-menu-creditcard' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/GogoCard"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','信用卡_@GoGo卡');" > @GoGo卡 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/FlygoCard"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','信用卡_FlyGo卡');" > FlyGo卡 < /a></li >
  <
  /ul> < /
  div > <
  /div> < /
  div > <
  /li> <
li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-investment i-icon menu-icon' > < /i><span>投資</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  div class = 'menu-item img-menu-coininvest' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/CoinInvestment"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','投資_銅板投資');" > 銅板投資 < i class = 'icons-ic-tag-new i-icon' > < /i></a > < /li> <
li > < a href = "/TSDIB_RichartWeb/Products/RichartInvestment"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','投資_理查投組');" > 理查投組 < /a></li >
  <
  /ul> < /
  div > <
  /div> < /
  div > <
  /li> <
li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-insure i-icon menu-icon' > < /i><span>保險</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div>  <
div class = 'menu-sub menu-center keep-right' >

  <
  div class = 'menu-item two-sort img-menu-insurance' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/AnnuityInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_年金險');" > 年金險 < i class = 'icons-ic-tag-new i-icon' > < /i></a > < /li> <
li > < a href = "/TSDIB_RichartWeb/Products/LifeInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_壽險');" > 壽險 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/TravelInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_旅行平安險');" > 旅行平安險 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/CarInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_汽車險');" > 汽車險 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/MotoInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_機車險');" > 機車險 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Products/BelongingsInsurance"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','保險_個人物品險');" > 個人物品險 < /a></li >
  <
  li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> < /
  ul > <
  /div> < /
  div > <
  /div> < /
  li > <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-loan i-icon menu-icon' > < /i><span>貸款</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center keep-right' >
  <
  div class = 'menu-item img-menu-loan' >
  <
  ul >


  <
  li > < a href = "/TSDIB_RichartWeb/Products/AllPersonalLoan"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','貸款_信貸');" > 信貸 < i class = 'icons-ic-tag-new i-icon' > < /i></a > < /li> <
li > < a href = "/TSDIB_RichartWeb/Products/CarLoan"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊產品服務內容按鈕','貸款_車貸');" > 車貸 < i class = 'icons-ic-tag-new i-icon' > < /i></a > < /li> < /
  ul > <
  /div> < /
  div > <
  /div> < /
  li > <
  /ul> <
ul class = 'navbar-nav align-items-lg-center m-floor m-other' >
  <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i
class = 'icons-ic-menu-m-knowyou i-icon menu-icon' > < /i><span>Richart煉金攻略</span > < i
class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  div class = 'menu-item img-menu-knowyou' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Group?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart煉金攻略內容按鈕','旅遊冒險家');goGroup(0)" > 旅遊冒險家 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Group?target=1"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart煉金攻略內容按鈕','社會初心者');goGroup(1)" > 社會初心者 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Group?target=2"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart煉金攻略內容按鈕','職場勇士');goGroup(2)" > 職場勇士 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Group?target=3"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart煉金攻略內容按鈕','家庭隊長');goGroup(3)" > 家庭隊長 < /a></li >
  <
  li class = 'null-value' > < /li> < /
  ul > <
  /div> < /
  div > <
  /div> < /
  li > <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-service i-icon menu-icon' > < /i><span>Richart 會什麼</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  div class = 'menu-item three-sort img-menu-goodat' >
  <
  ul >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/Expertise/Management?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','聰明管錢');goManage(0)" > 聰明管錢 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Expertise/Management?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','聰明管錢_登入前預覽');goManage(0)" > 登入前預覽 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Expertise/Management?target=1"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','聰明管錢_帳務分析');goManage(1)" > 帳務分析 < /a></li >
  <
  li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> <
li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/Expertise/Payment?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','輕鬆收付');goExpertise(0)" > 輕鬆收付 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Expertise/Payment?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','輕鬆收付_繳費');goExpertise(0)" > 繳費 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Expertise/Payment?target=1"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','輕鬆收付_任意轉');goExpertise(1)" > 任意轉 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Expertise/Payment?target=2"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','輕鬆收付_無卡提款');goExpertise(2)" > 無卡提款 < /a></li >
  <
  li class = 'null-value' > < a href = '#' > < /a></li >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/Expertise/Introduce"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊Richart會什麼內容按鈕','Richart是誰');" > Richart 是誰？ < /a></li >
  <
  li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> <
li class = 'null-value' > < /li> < /
  ul > <
  /div> < /
  div > <
  /div> < /
  li > <
  li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-teach i-icon menu-icon' > < /i><span>教學指南</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center' >
  <
  div class = 'menu-item two-sort img-menu-teach' >
  <
  ul >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/Guide"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','手把手教學');" > 手把手教學 < /a></li > < !--2020 - 05 - 28 Mantis0012928 Rachel-- >
  <
  li class = 'd-none d-lg-block text-grey xl-width' > < a href = "/TSDIB_RichartWeb/Guide/Beginner?target=0"
onclick = "goCarousel('tsdib_richartwebguideBeginner',0);ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','手把手_10分鐘完成開戶');" > 10 分鐘完成開戶 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey xl-width' > < a href = "/TSDIB_RichartWeb/Guide/Beginner?target=2"
onclick = "goCarousel('tsdib_richartwebguideBeginner',2);ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','手把手_升級帳戶轉帳不受限');" > 升級帳戶轉帳不受限 < /a></li >
  <
  li class = 'd-none d-lg-block text-grey xl-width' > < a href = "/TSDIB_RichartWeb/Guide/DemandDeposit?target=0"
onclick = "goCarousel('tsdib_richartwebguideDemandDeposit',0);ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','手把手_設定每月自動轉入(ACH)');" > 設定每月自動轉入(ACH) < /a></li >
  <
  li class = 'd-none d-lg-block text-grey' > < a href = "/TSDIB_RichartWeb/Guide"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','手把手_看全部教學');" > 看全部教學 < /a></li >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/School/FAQPage"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','常見問題Q&A');clearFAQData();" > 常見問題 Q & A < /a></li >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/School/Cards?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','狗狗卡比較');goCards(0)" > 狗狗卡比較 < /a></li >
  <
  li class = 'lg-bold' > < a href = "/TSDIB_RichartWeb/School/RichartPoint?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','R幣專區');goRCoin(0)" > R 幣專區 < /a></li >
  <
  li class = "lg-bold tmp-open-help" > < a onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊教學指南內容按鈕','智能客服');" > 智能客服 < /a></li >
  <
  li class = 'null-value' > < /li>

  <
  /ul> < /
  div > <
  /div> < /
  div > <
  /li> <
li id = 'menuNews' >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project menu-link' > < i class = 'icons-ic-menu-m-news i-icon menu-icon' > < /i>

  <
  a href = "/TSDIB_RichartWeb/RC02/RC020205"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊最新消息按鈕','true');removeNews()"
class = 'menu-link-color menu-lg-news' > < span > 最新消息 < /span></a > < /div> <
div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center keep-right menu-news-mob d-none' >
  <
  div class = 'menu-item news-sort' >
  <
  div class = 'menu-news-box' >
  <
  div class = 'menu-news-title' > < i class = 'icons-ic-title-line i-icon' > < /i><span>熱門活動</span > < /div> <
div class = 'news-content' >
  <
  a href = '#' > < h4 > 2019 / 06 / 14 - 2019 / 12 / 31 < /h4> <
div class = 'd-md-flex align-items-start' >
  <
  p > 悠遊Debit卡讓你自由消費！ 符合活動條件享悠遊卡最高10 % 回饋 < /p> <
img width = '116'
height = '62'
src = "/TSDIB_RichartWeb/static/revamp/images/img-menu-news.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-menu-news@2x.png 2x" / > < /div> < /
  a > <
  /div> <
div class = 'menu-news-title' > < i class = 'icons-ic-title-line i-icon' > < /i><span>公告</span > < /div> <
div class = 'news-content' >
  <
  a href = '#' > < h4 > 2019 / 07 / 16 < /h4> <
div class = 'd-md-flex align-items-start' >
  <
  p > 【Richart校園獎學金， 月月抽千元現金】 - 7 月份中獎公告 < /p> < /
  div > <
  /a> < /
  div > <
  div class = 'news-content' >
  <
  a href = '#' > < h4 > 2019 / 06 / 28 < /h4> <
div class = 'd-md-flex align-items-start' > < p > Richart新臺幣活期存款專案調整公告 < /p></div >
  <
  /a> < /
  div > <
  div class = 'text-center news-button' >
  <
  a href = '#' > 查看全部 < /a> < /
  div > <
  /div> < /
  div > <
  /div> < /
  div > <
  /li> <
li >
  <
  div class = 'menu-card' >
  <
  div class = 'menu-project' > < i class = 'icons-ic-menu-m-setting i-icon menu-icon' > < /i><span>帳戶設定</span > < i class = 'icons-ic-arrow-list-mobile-down i-icon menu-arrow' > < /i></div >
  <
  div class = 'menu-triangle' > < /div> <
div class = 'menu-sub menu-center keep-right' >
  <
  div class = 'menu-item long-sort img-menu-account' >
  <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC050900"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','Richart金融卡換發');" > Richart 金融卡換發 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC050200"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','忘記代號或密碼');" > 忘記代號或密碼 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC050101"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','設定存款自動轉入服務(ACH)');" > 設定存款自動轉入服務(ACH) < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC050800"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','設定帳戶升級(轉帳/權益升級)');" > 設定帳戶升級(轉帳 / 權益升級) < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC050500"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','設定帳戶自動扣繳信用卡費');" > 設定帳戶自動扣繳信用卡費 < /a></li >
  <
  li > < a href = "/TSDIB_RichartWeb/RC05/RC051100"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊帳戶設定按鈕','開啟Richart變更綁定裝置功能');" > 開啟Richart變更綁定裝置功能 < /a></li >
  <
  /ul> < /
  div > <
  /div> < /
  div > <
  /li> < /
  ul > <
  /div> < /
  div > <
  /nav> < /
  header >


  <
  !--header 導覽列 End-- >

  <
  input type = "hidden"
id = "pageTitle"
value = "V2_產品服務_貸款_車貸" >
  <
  !--2021 - 03 - 23 Mantis #15706 GA/請協助新增新官網信貸、車貸GA Curitis -->



	<input type= "hidden"
id = "eventTitle"
value = "V2_產品服務_貸款_車貸_點擊Q&A展開按鈕" >
  <
  input type = "hidden"
id = "seeMoreTitle"
value = "V2_產品服務_貸款_車貸_點擊看更多Q&A按鈕" >
  <
  !--main 中間內容 Start-- >
  <
  main class = "" >
  <
  div class = "product-bg-upper" >
  <
  !--產品介紹 Start-- >
  <
  div class = "product-introduction" >
  <
  div class = "product-bg" >
  <
  svg width = "1385px"
height = "641px"
viewBox = "0 0 1385 700"
version = "1.1"
xmlns = "http://www.w3.org/2000/svg"
xmlns: xlink = "http://www.w3.org/1999/xlink" >
  <
  defs >
  <
  !--12 / 27 svg 形狀更新-- >
  <
  path
d = "M111.3,0 C37.6,61.2 0.8,132.2 0.986345798,213 C1.5,419.9 182.2,543.4 351,593 C636.4,676.9 1024.88888,651.311183 1224,517.971738 C1332.96392,445.001487 1386,338.7 1386,220.2 C1386,101.7 1332,28.2 1224,0 L111.3,0 Z"
id = "out-shape" > < /path> < /
  defs > <
  g id = "Page-1"
stroke = "none"
stroke - width = "1"
fill = "none"
fill - rule = "evenodd" >
  <
  g id = "banner_BG" >
  <
  mask id = "mask-2"
fill = "white" >
  <
  use transform = "translate(692.501646, 319.949313) scale(-1, 1) translate(-692.501646, -319.949313)"
xlink: href = "#out-shape" > < /use> < /
  mask > <
  use id = "Mask"
fill = "#00A8B8"
transform = "translate(692.501646, 319.949313) scale(-1, 1) translate(-692.501646, -319.949313)"
xlink: href = "#out-shape" > < /use> <
path
d = "M-83.0363624,430.447586 C-163.318207,216.585162 -136.093604,95.3965582 -1.36255462,66.8817741 C315.266695,-0.130356307 262.36077,294.391939 432.175534,328.429119 C527.489632,347.533601 697.900328,349.887158 738.310332,430.447586 C804.136699,561.677472 732.779508,658.194943 524.23876,720 L-52.7609921,679.992758 L-83.0363624,430.447586 Z"
id = "in-shape"
fill = "#00A1B8"
mask = "url(#mask-2)" > < /path> < /
  g > <
  /g> < /
  svg > <
  /div> <
div class = "container max-w-auto" >
  <
  div class = "banner-info d-md-flex d-sm-block" >
  <
  div class = "introduction flex-fill align-self-center" >
  <
  h1 class = "banner-title fade-in-1" >
  <
  div > Richart 車貸 < /div> <
div > 貸你邁向移動新方式 < /div> < /
  h1 > <
  !--2021 - 04 - 19 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



                            <div class= "banner-desc fade-in-2" > APP 申請、 免出門、 快速撥款 < span class = "d-inline-block" > 最高可貸 NT250 萬元 < /span></div >
  <
  !--2021 - 03 - 23 Mantis #15706 GA/請協助新增新官網信貸、車貸GA -->



                            <!-- 2021-04-26 Mantis# 0016172 新代理商行銷碼事件埋設(春樹) by May-- >
  <
  a onclick = "ga_trace_event('V2_產品服務_貸款_車貸','V2_產品服務_貸款_車貸_點擊主標立即開戶線上貸按鈕','true');SEM_report_conversion(web_openaccount_page); GDN_report_conversion(web_openaccount_page); VD_report_conversion(web_openaccount_page); AI_report_conversion(web_openaccount_page); entry_report_conversion(web_openaccount_page);"
href = "/TSDIB_RichartWeb/Redirect/OpenAccountLink"
class = "btn full-red fade-in-3" > 立即開戶線上貸 < /a> < /
  div > <
  div class = "banner-img" >
  <
  img class = "d-none d-md-block"
src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-kv-car-loan.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-kv-car-loan@2x.png 2x"
alt = "" >
  <
  img class = "d-md-none d-sm-inline-block"
src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-kv-car-loan-m.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-kv-car-loan-m@2x.png 2x"
alt = "" >
  <
  /div> < /
  div > <
  div class = "product-info fade-in-2" >
  <
  div class = "row" >
  <
  div class = "col-12 col-md-4 ct" >
  <
  i class = "icons-img-feature-highloan-quick i-icon" > < /i>

  <
  div class = "ct-title" > 申貸超迅速 < br > 最快 24 小時撥款 < /div> <
div class = "ct-desc" >
  <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i>

  <
  p class = "" > 登入 APP 即可申貸， 最快 24 小時撥款 < /p> < /
  div > <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <!--2021 - 04 - 19 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



                                        <p class= "" > 買車貸 NT15萬－ NT250萬 < /p> < /
  div > <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 買重機貸 NT15萬－ NT100萬 < /p> < /
  div > <
  /div> < /
  div > <
  div class = "col-12 col-md-4 ct" >
  <
  i class = "icons-img-feature-lowestfee i-icon" > < /i> <
div class = "ct-title" > 市場超優開辦費 < br > 各項規費一次繳清 < /div> <
div class = "ct-desc" >
  <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 買車貸、 買重機貸的開辦費最低 NT$1, 900 起 < /p> < /
  div > <
  div class = "ct-txt"
style = "width:275px" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 開辦費未包含監理站動產擔保抵押設定費 NT$900 唷 < /p> < /
  div > <
  /div> < /
  div > <
  div class = "col-12 col-md-4 ct" >
  <
  i class = "icons-img-feature-maji i-icon" > < /i> <
div class = "ct-title" > Richart 麻吉 < br > 享優惠 < /div> <
div class = "ct-desc" >
  <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 麻吉指數 0 - 69 分， 每提升 10 分， 開辦費減免 NT50 元 < /p> < /
  div > <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 麻吉指數 70 - 100 分， 每提升 10 分， 開辦費減免 NT100 元 < /p> < /
  div > <
  div class = "ct-txt" >
  <
  i class = "icons-ic-star-24-px" > < /i> <
p class = "" > 最多可減免開辦費 NT700 元 < /p> < /
  div > <
  /div> < /
  div > <
  /div> <
div class = "row bg-ft" >
  <
  div class = "col-12 col-md-4 ft" >
  <
  div class = "ft-list" >
  <
  i class = "icons-check-40-px i-icon" > < /i> <
p class = "" > 3 步驟快速申請 < /p> <
div class = "ft-txt" > 確認額度、 期數， APP 填寫申請表直接申貸， 三步驟即可完成！ < /div> < /
  div > <
  /div> <
div class = "col-12 col-md-4 ft" >
  <
  div class = "ft-list" >
  <
  i class = "icons-check-40-px i-icon" > < /i> <
p class = "" > 直接試算月付金 < /p> <
div class = "ft-txt" > 試算每月應付金額， 簡單做好財務管理！ < /div> < /
  div > <
  /div> < /
  div > <
  /div> < /
  div > <
  /div> <!--產品介紹 End-- >
  <
  !--試算 Start-- >
  <
  div class = "product-trial-loan" >
  <
  div class = "container max-w-auto position-relative" >
  <
  div class = "ct-bg tag-carousel" >
  <
  div class = "ct-bubble"
data - aos = "fade-up"
data - aos - duration = "1500"
data - aos - once = "true" >
  <
  h2 class = "lg-title" > 立即試算搶優貸 < br > Richart 讓你輕鬆用錢！ < /h2> <
div class = "tag-area" >
  <
  div class = "tag-carousel-tabs" >
  <
  div class = "item active" >
  <
  div class = "txt" > 買車貸 < /div> < /
  div > <
  div class = "item" >
  <
  div id = "heavyMotoLoan"
class = "txt" > 買重機貸 < /div> < /
  div > <
  /div> <
div class = "mt-3 text-center text-md-left" >
  <
  !--#modalLoanCompare-- >
  <
  a href = "#"
data - toggle = "modal"
data - target = "#modalLoanCompare"
class = "font-bold d-inline-flex align-items-center" > 看方案比較 < i class = "icons-ic-arrow-list-right i-icon ml-2" > < /i></a >
  <
  /div> < /
  div > <
  /div> <
div class = "tag-carousel-content" >
  <
  !--買車貸-- >
  <
  div class = "ct-dialog fade-in-show car" >
  <
  div class = "ct-trial" >
  <
  div class = "ct-box" >
  <
  div class = "info" >
  <
  div class = "" > 申請買車貸 < span class = "text-red" > NT < /span><span class="text-red" id="amount">50</span > < span class = "text-red" > 萬 < /span>，貸 <span class="text-red" id="carLoanYear">24</span > 個月 < br > 每月只要還款 < span class = "text-red" > NT < /span><span class="text-red" id="carLoanPayment">21,464</span > 元起 < /div> <
div id = "carLoanDesc"
class = "desc" > 年利率 2.88％ 起， 開辦費 NT1, 900 元起 < /div> < /
  div > <
  div class = "form-group mb-3" >
  <
  div class = "" >
  <
  div class = "form-radio-inline" >
  <
  input class = ""
type = "radio"
name = "carOptions"
id = "car1"
value = "option1"
checked >
  <
  label class = "form-check-label"
for = "car1" > 新車 < /label> < /
  div > <
  div class = "form-radio-inline" >
  <
  input class = ""
type = "radio"
name = "carOptions"
id = "car2"
value = "option2" >
  <
  label class = "form-check-label"
for = "car2" > 中古車 < /label> < /
  div > <
  /div> < /
  div > <
  div class = "loan-amount" >
  <
  div class = "cd-slider-tip" > 貸款金額 < /div> <
div id = "cdCarSliderAmount"
class = "cd-slider" > < /div> <
div class = "cd-slider-tip" >
  <
  div class = "mr-auto" > 15 萬 < /div> <!--2021 - 04 - 19 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



                                                <div class= "" > 250 萬 < /div> < /
  div > <
  /div> <
div class = "loan-year" >
  <
  div class = "cd-slider-tip" > 貸款年限 < /div> <
div id = "cdCarSliderYear"
class = "cd-slider" > < /div> <
div class = "cd-slider-tip" >
  <
  div class = "mr-auto" > 18 個月 < /div> <
div class = "carLoanMaxMonth" > 84 個月 < /div> < /
  div > <
  /div> <
div class = "text-center mt-3" >
  <
  !--2021 - 03 - 23 Mantis #15706 GA/請協助新增新官網信貸、車貸GA -->



                                        	<!-- 2021-04-26 Mantis# 0016172 新代理商行銷碼事件埋設(春樹) by May-- >
  <
  a onclick = "ga_trace_event('V2_產品服務_貸款_車貸','V2_產品服務_貸款_車貸_買車貸_點擊立即開戶線上貸按鈕','true');SEM_report_conversion(web_openaccount_page); GDN_report_conversion(web_openaccount_page); VD_report_conversion(web_openaccount_page); AI_report_conversion(web_openaccount_page); entry_report_conversion(web_openaccount_page);"
href = "/TSDIB_RichartWeb/Redirect/OpenAccountLink"
class = "btn full-red" > 立即開戶線上貸 < /a> < /
  div > <
  /div> < /
  div > <
  /div> <!--買重機貸-- >
  <
  div class = "ct-dialog fade-in-show heavy d-none" >
  <
  div class = "ct-trial" >
  <
  div class = "ct-box" >
  <
  div class = "info" >
  <
  div class = "" > 申請買重機貸 < span class = "text-red" > NT < /span><span class="text-red" id="heavyMotoAmount">30</span > < span class = "text-red" > 萬 < /span>，貸 <span class="text-red" id="heavyMotoYear">24</span > 個月 < br > 每月只要還款 < span class = "text-red" > NT < /span><span class="text-red" id="heavyLoanPayment">13,161</span > 元起 < /div> <
div id = "heavyDesc"
class = "desc" > 年利率 5％ 起， 開辦費 NT1, 900 元起 < /div> < /
  div > <
  div class = "form-group mb-3" >
  <
  div class = "" >
  <
  div class = "form-radio-inline" >
  <
  input class = ""
type = "radio"
name = "heavyOptions"
id = "heavy1"
value = "option1"
checked >
  <
  label class = "form-check-label"
for = "heavy1" > 新車 < /label> < /
  div > <
  div class = "form-radio-inline" >
  <
  input class = ""
type = "radio"
name = "heavyOptions"
id = "heavy2"
value = "option2" >
  <
  label class = "form-check-label"
for = "heavy2" > 中古車 < /label> < /
  div > <
  /div> < /
  div > <
  div class = "loan-amount" >
  <
  div class = "cd-slider-tip" > 貸款金額 < /div> <
div id = "cdHeavySliderAmount"
class = "cd-slider" > < /div> <
div class = "cd-slider-tip" >
  <
  div class = "mr-auto" > 15 萬 < /div> <
div class = "" > 100 萬 < /div> < /
  div > <
  /div> <
div class = "loan-year" >
  <
  div class = "cd-slider-tip" > 貸款年限 < /div> <
div id = "cdHeavySliderYear"
class = "cd-slider" > < /div> <
div class = "cd-slider-tip" >
  <
  div class = "mr-auto" > 18 個月 < /div> <
div class = "heavyLoanMaxMonth" > 60 個月 < /div> < /
  div > <
  /div> <
div class = "text-center mt-3" >
  <
  !--2021 - 03 - 23 Mantis #15706 GA/請協助新增新官網信貸、車貸GA -->



                                        	<!-- 2021-04-26 Mantis# 0016172 新代理商行銷碼事件埋設(春樹) by May-- >
  <
  a onclick = "ga_trace_event('V2_產品服務_貸款_車貸','V2_產品服務_貸款_車貸_買重機貸_點擊立即開戶線上貸按鈕','true');SEM_report_conversion(web_openaccount_page); GDN_report_conversion(web_openaccount_page); VD_report_conversion(web_openaccount_page); AI_report_conversion(web_openaccount_page); entry_report_conversion(web_openaccount_page);"
href = "/TSDIB_RichartWeb/Redirect/OpenAccountLink"
class = "btn full-red" > 立即開戶線上貸 < /a> < /
  div > <
  /div> < /
  div > <
  /div> < /
  div > <
  /div> <
div class = "bg-interactive-02" > < img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/bg-interactive-02.png"
alt = "" > < /div> < /
  div > <
  /div> <!--試算 End-- >
  <
  /div> <
div class = "product-bg-news" >
  <
  !--app and line tab Start-- >
  <
  div class = "product-tab-phone" >
  <
  div class = "container max-w-auto" >
  <
  div class = "row" >
  <
  div class = "col-info-width d-flex h-auto"
data - aos = "fade-up"
data - aos - duration = "1500"
data - aos - once = "true" >
  <
  div class = "d-flex h-auto" >
  <
  div class = "" >
  <
  h2 class = "lg-title" > 立即試算搶優貸 < br > Richart 讓你輕鬆用錢！ < /h2> <!--膠囊 tab Start-- >
  <
  !-- < div class = "title-capsule" >
  <
  a class = "item active" > APP 功能 < /a> <
a class = "item" > LINE 服務 < /a> < /
  div > -- > <
  !--膠囊 tab End-- >
  <
  !--app 圓圈 tab Start-- >
  <
  div class = "title-circle appCircle" >
  <
  div class = "row justify-content-center justify-content-lg-start" >
  <
  div class = "col-3" >
  <
  div class = "item active" >
  <
  div class = "txt" >
  <
  i class = "icons-ic-tab-calculator i-icon ic-active" > < /i> <
i class = "icons-ic-tab-calculator-n i-icon ic-default" > < /i> <
div class = "desc" > < span class = "mx-auto" > 貸款試算 < /span></div >
  <
  /div> < /
  div > <
  /div> <
div class = "col-3" >
  <
  div class = "item" >
  <
  div class = "txt" >
  <
  i class = "icons-ic-tab-overview i-icon ic-active" > < /i> <
i class = "icons-ic-tab-overview-n i-icon ic-default" > < /i> <
div class = "desc" > < span class = "mx-auto" > 貸款總覽 < /span></div >
  <
  /div> < /
  div > <
  /div> < /
  div > <
  /div> <!--app 圓圈 tab End-- >
  <
  !--標題文字 Start-- >
  <
  div class = "tab-info tab-info-loan appTabInfo" >
  <
  !--app 貸款試算 01-- >
  <
  div class = "show" >
  <
  div class = "caption" > 貸款試算 < /div> <
div class = "desc" > Richart 預先提供專屬貸款方案， 預估可貸額度、 手續費及利率資訊通通透明呈現， 讓你申貸安心， 財務管理更方便。 < /div> < /
  div > <
  !--app 貸款總覽 02-- >
  <
  div >
  <
  div class = "caption" > 貸款總覽 < /div> <
div class = "desc" > 完成撥貸後， 只要登入 APP 即可確認貸款狀態及還款進度， 資訊超透明。 < /div> < /
  div > <
  /div> <!--標題文字 End-- >
  <
  /div> < /
  div > <
  /div> <
div class = "col-phone-width d-flex h-auto" >
  <
  div class = "d-flex h-auto w-100 align-items-end" >
  <
  div class = "w-100" >
  <
  !--手機圖片 Start-- >
  <
  div class = "tab-img" >
  <
  div class = "phone" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-phone.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-phone@2x.png 2x"
alt = "" >
  <
  /div> <
div class = "screen owl-carousel app-carousel owl-theme show" >
  <
  !--app 貸款試算 screen 01-- >
  <
  div >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-carloan-screen-01.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-carloan-screen-01@2x.png 2x"
alt = "" >
  <
  /div> <!--app 貸款總覽 screen 02-- >
  <
  div >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-carloan-screen-02.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-carloan-screen-02@2x.png 2x"
alt = "" >
  <
  /div> < /
  div > <
  div class = "dog appDog" >
  <
  !--app 貸款試算 dog 01-- >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-product-car-loan-app-1.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-product-car-loan-app-1@2x.png 2x"
alt = ""
style = "display: block;" >
  <
  !--app 貸款總覽 dog 02-- >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-product-car-loan-app-2.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-product-car-loan-app-2@2x.png 2x"
alt = "" >
  <
  /div> <
div class = "img-highloan-info" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-info-highloan-app.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-info-highloan-app@2x.png 2x"
alt = "" >
  <
  /div> <
div class = "bg-circle" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-intro-carloan.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-intro-carloan@2x.png 2x"
class = "img-intro-foreign" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-intro-carloan-m.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-intro-carloan-m@2x.png 2x"
class = "img-intro-foreign-m" >
  <
  /div> < /
  div > <
  !--手機圖片 End-- >
  <
  /div> < /
  div > <
  /div> <
div class = "bg-productpage-news-m" > < img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/bg-productpage-interactive-m.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/bg-productpage-interactive-m@2x.png 2x"
alt = "" >
  <
  /div> < /
  div > <
  /div> < /
  div > <
  !--app and line tab End-- >
  <
  div class = "bg-productpage-news" > < /div> < /
  div > <
  !--Richart 車貸重要公告 Start-- >







  <
  div class = "product-news"
data - aos = "fade-up"
data - aos - duration = "1500"
data - aos - once = "true" >
  <
  div class = "container max-w-auto" >
  <
  div class = "lg-title-outer" >
  <
  h2 class = "lg-title" > 車貸重要公告 < img class = "img-notice-stars"
src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-notice-stars.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-notice-stars@2x.png 2x"
alt = "" > < /h2> < /
  div > <
  div class = "news-mx--15" >
  <
  div class = "owl-carousel owl-theme product-carousel" >

  <
  a href = "/TSDIB_RichartWeb/RC02/RC020201?announceNo=19028"
class = "item" >
  <
  div class = "owl-card" >
  <
  div class = "date" > < span class = "" > 2021 / 06 / 07 < /span>-<span class="">2021/
10 / 06 < /span> < /
  div > <
  div class = "info" > Richart貸你趴趴造 核貸送禮再抽加油金 < /div> <!-- < a href = ""
class = "link" > 好康優惠 < /a> --> < /
  div > <
  /a>

  <
  /div> < /
  div > <
  /div> < /
  div >

  <
  !--Richart 車貸重要公告 End-- >
  <
  !--Q & A Start-- >
  <
  div id = "walkthrouTemplate"
class = "d-none" >

  <
  /div>







  <
  div class = "product-qa"
data - aos = "fade-up"
data - aos - duration = "1500"
data - aos - once = "true" >
  <
  div class = "container max-w-auto position-relative" >
  <
  div class = "img-title" >
  <
  div class = "qa-title" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-qa.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-qa@2x.png 2x" >
  <
  div class = "q-mark" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/q-symbol.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/q-symbol@2x.png 2x" >
  <
  /div> <
div class = "qa-dog" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/qa-dog.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/qa-dog@2x.png 2x" >
  <
  /div> < /
  div > <
  /div> <!--2020 - 02 - 18 Mantis0012047 Rachel： add-- >
  <
  div class = "qa-box" >
  <
  div class = "qa-padding" >
  <
  div class = "accordion accordion-qa"
id = "accordionForeignQa" >

  <
  !--2020 - 05 - 05 Sprint76 GA #26 Rachel & GA# 27 Curitis-- >
  <
  div class = "qa-title"
id = "heading1"
onclick = "ga_trace_event_click(this);" >
  <
  a href = ""
class = ""
data - toggle = "collapse"
data - target = "#collapse1"
aria - expanded = "false"
aria - controls = "collapse1" >
  <
  div class = "d-flex" >
  <
  span class = "num" > Q1. < /span> <
span class = "txt  flex-grow-1" > Richart車貸申辦方式為何？ < /span> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapse1"
class = "collapse"
aria - labelledby = "heading1"
data - parent = "#accordionForeignQa" >
  <
  div class = "qa-body" >
  <
  ul >
  <
  li > 需準備身分證、 購車證明(購車訂單或買賣合約書)。 < /li> <
li > 於Richart App登入後→ 點擊「 總覽」→「 貸款」→「 汽車貸」 或「 重機貸」。 < /li> <
li > 確認金額、 期數、 填寫申請資料， 即可完成申請步驟， 接下來會有專人聯繫。 < /li>※
貼心小提醒： 完成Richart麻吉指數任務可以獲得更優惠的手續費喔！
  <
  /div> < /
  div >


  <
  !--2020 - 05 - 05 Sprint76 GA #26 Rachel & GA# 27 Curitis-- >
  <
  div class = "qa-title"
id = "heading2"
onclick = "ga_trace_event_click(this);" >
  <
  a href = ""
class = ""
data - toggle = "collapse"
data - target = "#collapse2"
aria - expanded = "false"
aria - controls = "collapse2" >
  <
  div class = "d-flex" >
  <
  span class = "num" > Q2. < /span> <
span class = "txt  flex-grow-1" > Richart車貸會收開辦費嗎 ? < /span> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapse2"
class = "collapse"
aria - labelledby = "heading2"
data - parent = "#accordionForeignQa" >
  <
  div class = "qa-body" >
  完成Richart麻吉指數任務可以獲得更優惠的開辦費， 開辦費最低只需NT1, 900 元 < br > ※貼心提醒、 另須酌收動產擔保抵押設定費NT900元 <
  /div> < /
  div >


  <
  !--2020 - 05 - 05 Sprint76 GA #26 Rachel & GA# 27 Curitis-- >
  <
  div class = "qa-title"
id = "heading3"
onclick = "ga_trace_event_click(this);" >
  <
  a href = ""
class = ""
data - toggle = "collapse"
data - target = "#collapse3"
aria - expanded = "false"
aria - controls = "collapse3" >
  <
  div class = "d-flex" >
  <
  span class = "num" > Q3. < /span> <
span class = "txt  flex-grow-1" > 我可以繳交甚麼資料作為財力證明？ < /span> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapse3"
class = "collapse"
aria - labelledby = "heading3"
data - parent = "#accordionForeignQa" >
  <
  div class = "qa-body" >
  可提供薪轉存摺內頁 / 封面、 勞保金額、 扣繳憑單、 薪資單…… 等， 做為財力證明喔!
  <
  /div> < /
  div >


  <
  !--2020 - 05 - 05 Sprint76 GA #26 Rachel & GA# 27 Curitis-- >
  <
  div class = "qa-title"
id = "heading4"
onclick = "ga_trace_event_click(this);" >
  <
  a href = ""
class = ""
data - toggle = "collapse"
data - target = "#collapse4"
aria - expanded = "false"
aria - controls = "collapse4" >
  <
  div class = "d-flex" >
  <
  span class = "num" > Q4. < /span> <
span class = "txt  flex-grow-1" > Richart貸款是怎麼計息的？ 我要去哪邊查詢？ < /span> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapse4"
class = "collapse"
aria - labelledby = "heading4"
data - parent = "#accordionForeignQa" >
  <
  div class = "qa-body" >
  Richart貸款計息方式係按年利率計算， 按日計息， 1 年（ 含閏年） 以365日為計息基礎， 以每日最終貸款餘額乘以年利率， 再除以365即得每日之利息額。 <
  br >
  總費用年百分率3 .14 % ~16.39 %
  <
  /div> < /
  div >


  <
  !--2020 - 05 - 05 Sprint76 GA #26 Rachel & GA# 27 Curitis-- >
  <
  div class = "qa-title"
id = "heading5"
onclick = "ga_trace_event_click(this);" >
  <
  a href = ""
class = ""
data - toggle = "collapse"
data - target = "#collapse5"
aria - expanded = "false"
aria - controls = "collapse5" >
  <
  div class = "d-flex" >
  <
  span class = "num" > Q5. < /span> <
span class = "txt  flex-grow-1" > 我可以提前還款嗎？ < /span> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapse5"
class = "collapse"
aria - labelledby = "heading5"
data - parent = "#accordionForeignQa" >
  <
  div class = "qa-body" >
  <
  ul >
  <
  li > 大口貸: 有綁約18期， 因此可提前還款但無法提前結清。 < /li> <
li > 小口貸和一口貸: 免綁約， 可提前還款提前結清。 < /li> <
li > 汽、 機車貸： 有綁約12期， 因此可提前還款但無法提前結清。 < /li>
總費用年百分率3 .14 % ~16.39 %
  <
  /ul> < /
  div > <
  /div>


  <
  /div> <
div class = "d-flex justify-content-end" >
  <
  !--2020 - 05 - 05 Spring76 GA #27 Curitis 點擊看更多Q&A GA記錄 -->



		                        <a href= "/TSDIB_RichartWeb/School/FAQPage?categoryId=childCategory10"
onclick = "ga_trace_event($('#pageTitle').val(),$('#seeMoreTitle').val(),'true');clearFAQData()"
class = "qa-link-more" > < span class = "align-middle mr-5px" > 看更多Q & A < /span> <i class="icons-ic-arrow-list-right i-icon"></i > < /a> < /
  div > <
  /div> <!--2020 - 03 - 17 mantis #12382 fix -->



			                <input type= "hidden"
id = "template"
value = "&lt;div class=&#034;qa-teach&#034;&gt; &
lt;
div class = & #034;md-title&# 034; & gt;
Richart 手把手教學 & lt;
/div&gt; &
lt;
div class = & #034;qa-teach-box&# 034; & gt; &
lt;
a href = & #034;/TSDIB_RichartWeb/Guide/PersonalLoan&# 034;
class = & #034;item&# 034; & gt; &
lt;
img class = & #034;img&# 034;
src = & #034;https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-teach-loan.png&# 034;
srcset = & #034;https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/img-teach-loan@2x.png 2x&# 034;
alt = & #034;&# 034; & gt; &
lt;
div class = & #034;desc&# 034; & gt;
如何申請貸款 & lt;
/div&gt; &
lt;
/a&gt; &
lt;
/div&gt; &
lt;
/div&gt;"> <
script type = "text/javascript" >
  document.write($("#template").val()); <
/script>  <
div class = "bg-productpage-qa" >
  <
  img src = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/bg-productpage-qa.png"
srcset = "https://richartweb-tsbank.cdn.hinet.net/TSDIB_RichartWeb/static/revamp/images/bg-productpage-qa@2x.png 2x"
alt = "" >
  <
  /div> < /
  div > <
  /div> < /
  div >

  <
  !--Q & A End-- >
  <
  !--最多人關注 Start-- >
  <
  div class = "product-attention" >
  <
  div class = "container max-w-auto" >
  <
  div class = "attention-box" >
  <
  h2 class = "lg-title" > 最多人關注 < /h2> <
div class = "attention-link" >
  <
  a href = "/TSDIB_RichartWeb/Products/AllPersonalLoan"
class = "item" >
  <
  span class = "align-middle" > 信貸 < /span> < /
  a > <
  /div> <
div class = "attention-desc" > 看看 < a href = "/TSDIB_RichartWeb/Expertise/Group?target=3"
onclick = "goGroup(3)"
class = "" > 家庭隊長 < /a>如何用 Richart 煉金</div >
  <
  /div> < /
  div > <
  /div> <!--最多人關注 End-- >
  <
  !--注意事項 Start-- >
  <
  div class = "index-notice" >
  <
  div class = "container" >
  <
  div class = "info" >
  <
  div class = "font-bold des-text-14" > 買車貸及買重機貸注意事項： < /div> <
ul class = "list-num" >
  <
  li > 請務必詳實填寫申請書， 以免影響申請額度權益。 < /li> <
li > 申貸過程中可先上傳身分證件及財力證明影像檔， 以加速申請進度。 < /li> <
li > 新車貸款 總費用年百分率說明: 貸款金額： NT$40萬元， 貸款期間： 5 年， 貸款利率： 2.88 % ~5 % ，各項相關費用總金額： NT$2, 600 元 / 筆， 總費用年百分率： 3.14 % ~5.27 % ；中古車貸款 總費用年百分率說明: 貸款金額： NT$40萬元， 貸款期間： 5 年， 貸款年利率： 4 % ~11 % ，
  各項相關費用總金額： NT$2, 600 元 / 筆， 總費用年百分率： 4.27 % ~11.28 % ；原車融資貸款 總費用年百分率說明: 貸款金額： NT$40萬元， 貸款期間： 5 年， 貸款年利率： 6.5 % ~12 % ，各項相關費用總金額： NT$5, 000 元 / 筆， 總費用年百分率： 7.02 % ~12.56 % 。 < /li> <
li > 本廣告揭露之年百分率係按主管機關備查之標準計算範例予以計算， 實際貸款條件仍以台新銀行提供之產品為準，
且每一客戶實際之年百分率仍視其個別貸款產品及授信條件而有所不同。 總費用年百分率不等於貸款利率。 本總費用年百分率之計算基準日為2020年11月20日。 < /li> <
li > 本行保留最終審核核貸與否、 貸款金額、 貸款利率及各項條件之權利。 < /li> <
li > 本行未與任何代辦業者或行銷公司合作辦理貸款事宜， 請您留意， 以免損害自身權益。 < /li> <
li > 申請過程中有任何疑問， 請於營業時間9： 00 AM至5： 30 PM， 洽詢汽車貸款專線0800 - 000 - 456 按2， 由專人為您服務。 < /li> < /
  ul > <
  /div> < /
  div > <
  /div> <!--注意事項 End-- >
  <
  /main> <!--main 中間內容 End-- >
  <
  !--Modal 看方案比較# modalLoanCompare Start-- >
  <
  div class = "modal fade product-modal"
id = "modalLoanCompare"
tabindex = "-1"
role = "dialog"
aria - labelledby = "modalLoanCompareTitle"
aria - hidden = "true" >
  <
  div class = "modal-dialog modal-dialog-centered"
role = "document" >
  <
  div class = "modal-content" >
  <
  div class = "m-header d-flex align-items-center" >
  <
  div class = "modal-title flex-fill"
id = "modalLoanCompareTitle" > 車貸方案比較 < /div> <
button type = "button"
data - dismiss = "modal"
aria - label = "Close"
class = "btn-close d-inline-block" >
  <
  i aria - hidden = "true"
class = "icons-ic-others-close i-icon ic-default" > < /i> <
i aria - hidden = "true"
class = "icons-ic-others-close-h i-icon ic-active" > < /i> < /
  button > <
  /div> <
div class = "m-body" >
  <
  !--大網表格(768 px and up) Start-- >
  <
  div class = "m-table m-table-compare d-none d-md-block mb-0" >
  <
  table class = "table th-top" >
  <
  tr >
  <
  th class = "text-left"
style = "width: 20%" > 方案特色 < /th> <
th style = "width: 40%" > 買車貸 < /th> <
th style = "width: 40%" > 買重機貸 < /th> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 貸款額度 < /td> <!--2021 - 04 - 19 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



                                <td>NT15萬-NT250萬</td>



                                <td>NT15萬-NT100萬</td>



                            </tr>



                            <tr>



                                <td class= "font-bold text-left" > 貸款期限 < /td> <
td > 18 個月 - 84 個月 < /td> <
td > 18 個月 - 60 個月 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 貸款利率 < /td> <
td > 2.88 % 起 < /td> <
td > 5 % 起 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 開辦費 < /td> <
td > NT1900 元起 < /td> <
td > NT1900 元起 < /td> < /
  tr > <
  /table> < /
  div > <
  !--大網表格(768 px and up) End-- >
  <
  !--小網表格(less than 767 px) Start-- >
  <
  div class = "d-md-none" >
  <
  div class = "accordion accordion-incar"
id = "accordionLoan" >
  <
  !--買車貸-- >
  <
  div class = "heading"
id = "headingHighLoan" >
  <
  a href = ""
data - toggle = "collapse"
data - target = "#collapseHighLoan"
aria - expanded = "false"
aria - controls = "collapseHighLoan" >
  <
  div class = "d-flex" >
  <
  div class = "txt flex-grow-1" > 買車貸 < /div> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapseHighLoan"
class = "collapse"
aria - labelledby = "headingHighLoan"
data - parent = "#accordionLoan" >
  <
  div class = "incar-body" >
  <
  div class = "m-table m-table-compare" >
  <
  table class = "table th-top" >
  <
  tr >
  <
  th class = "text-left"
colspan = "2" > 方案特色 < /th> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left"
width = "38%" > 貸款額度 < /td> <!--2021 - 04 - 19 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



                                                <td>NT15萬-NT250萬</td>



                                            </tr>



                                            <tr>



                                                <td class= "font-bold text-left" > 貸款期限 < /td> <
td > 13 - 60 期 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 貸款利率 < /td> <
td > 2.6 % 起 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 手續費 < /td> <
td > NT900 元起 < /td> < /
  tr > <
  /table> < /
  div > <
  /div> < /
  div > <
  !--買重機貸-- >
  <
  div class = "heading"
id = "headingLowLoan" >
  <
  a href = ""
data - toggle = "collapse"
data - target = "#collapseLowLoan"
aria - expanded = "false"
aria - controls = "collapseLowLoan" >
  <
  div class = "d-flex" >
  <
  div class = "txt flex-grow-1" > 買重機貸 < /div> <
div class = "p-icon" >
  <
  i class = "icons-ic-arrow-list-down i-icon ic-default" > < /i> <
i class = "icons-ic-arrow-list-up i-icon ic-active" > < /i> < /
  div > <
  /div> < /
  a > <
  /div> <
div id = "collapseLowLoan"
class = "collapse"
aria - labelledby = "headingLowLoan"
data - parent = "#accordionLoan" >
  <
  div class = "incar-body" >
  <
  div class = "m-table m-table-compare" >
  <
  table class = "table th-top" >
  <
  tr >
  <
  th class = "text-left"
colspan = "2" > 方案特色 < /th> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left"
width = "38%" > 貸款額度 < /td> <
td > NT15萬 - NT100萬 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 貸款期限 < /td> <
td > 13 - 60 期 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 貸款利率 < /td> <
td > 3.5 % 起 < /td> < /
  tr > <
  tr >
  <
  td class = "font-bold text-left" > 手續費 < /td> <
td > NT900 元起 < /td> < /
  tr > <
  /table> < /
  div > <
  /div> < /
  div > <
  /div> < /
  div > <
  !--小網表格(less than 767 px) End-- >
  <
  div > 開辦費未包含監理站動產擔保抵押設定費 NT900 元， 須另外收費唷! < /div> <
div > 實際方案內容請以 Richart APP 貸款內容為主。 < /div> < /
  div > <
  /div> < /
  div > <
  /div> <!--Modal 看方案比較# modalLoanCompare End-- >
  <
  script >
  $(document).ready(function () {
    //車貸中古車
    $("#car2").click(function () {
      $("#carLoanDesc").text("年利率 4％ 起，手續費 NT2,800 元起");
      initUsedCarLoanBar();
      $(".carLoanMaxMonth").text("60個月");
      //調整試算文字
      $("#amount").text("50");
      $("#carLoanYear").text("24");
      $("#carLoanPayment").text("21,712");

    });
    //車貸新車
    $("#car1").click(function () {
      $("#carLoanDesc").text("年利率 2.88％ 起，手續費 NT2,800 元起");
      initNewCarLoanBar();
      $(".carLoanMaxMonth").text("84個月");
      $("#amount").text("50");
      $("#carLoanYear").text("24");
      $("#carLoanPayment").text("21,464");
    });
    //重機新車
    $("#heavy1").click(function () {
      $("#heavyDesc").text("年利率 5％ 起，手續費 NT2,800 元起");
      initNewHeavyBar();
      $(".heavyLoanMaxMonth").text("60個月");
      //調整試算文字
      $("#heavyMotoAmount").text("30");
      $("#heavyMotoYear").text("24");
      $("#heavyLoanPayment").text("13,161");
    });
    //重機中古車
    $("#heavy2").click(function () {
      $("#heavyDesc").text("年利率 6.5％ 起，手續費 NT2,800 元起");
      initUsedHeavyBar();
      $(".heavyLoanMaxMonth").text("48個月");
      //調整試算文字
      $("#heavyMotoAmount").text("30");
      $("#heavyMotoYear").text("24");
      $("#heavyLoanPayment").text("13,364");
    });

    initNewCarLoanBar();
    initNewHeavyBar();

  })

  //車貸 中古車Bar init
  <
  !--2021 - 04 - 21 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



		function initUsedCarLoanBar(){



			var valueArray = getNumberArray(15, 1, 250);
var carLoanId = "cdCarSliderAmount";
var sliderBar = sliderBuilderForLoan(carLoanId)
  .set("valueArray", valueArray)
  .set("defaultValue", 35)
  .event("onSlide", function (val, index) {
    $("#amount").text(formatAmountforLoan(val));
    var month = parseInt($("#carLoanYear").text());
    var repayAmount = handingFeeCalculation(val * 10000, month, 4)
    $("#carLoanPayment").text(formatAmountforLoan(repayAmount));
    // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
    ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買車貸_拖拉貸款金額或年限', 'true');

  })
  .build();

var heavyLoanYearForCarArray = getNumberArray(18, 6, 60);
var heavyLoanYearForCarID = "cdCarSliderYear";
var sliderBarYearForCarLoans = sliderBuilderForLoan(heavyLoanYearForCarID)
  .set("valueArray", heavyLoanYearForCarArray)
  .set("defaultValue", 1)
  .event("onSlide", function (val, index) {
    $("#carLoanYear").text(formatYearforLoan(val));
    var amount = parseInt($("#amount").text());
    var repayAmount = handingFeeCalculation(amount * 10000, val, 4)
    $("#carLoanPayment").text(formatAmountforLoan(repayAmount));
    // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
    ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買車貸_拖拉貸款金額或年限', 'true');
  })
  .build();
}

//車貸 新車Bar init
<
!--2021 - 04 - 21 Mantis #0016096 車貸產品頁額度調整成250萬 by May  -->



		function initNewCarLoanBar(){



			var valueArray = getNumberArray(15, 1, 250);
var carLoanId = "cdCarSliderAmount";
var sliderBar = sliderBuilderForLoan(carLoanId)
  .set("valueArray", valueArray)
  .set("defaultValue", 35)
  .event("onSlide", function (val, index) {
    $("#amount").text(formatAmountforLoan(val));
    var month = parseInt($("#carLoanYear").text());
    var repayAmount = handingFeeCalculation(val * 10000, month, 2.88)
    $("#carLoanPayment").text(formatAmountforLoan(repayAmount));
    // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
    ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買車貸_拖拉貸款金額或年限', 'true');
  })
  .build();

var heavyLoanYearForCarArray = getNumberArray(18, 6, 84);
var heavyLoanYearForCarID = "cdCarSliderYear";
var sliderBarYearForCarLoans = sliderBuilderForLoan(heavyLoanYearForCarID)
  .set("valueArray", heavyLoanYearForCarArray)
  .set("defaultValue", 1)
  .event("onSlide", function (val, index) {
    $("#carLoanYear").text(formatYearforLoan(val));
    var amount = parseInt($("#amount").text());
    var repayAmount = handingFeeCalculation(amount * 10000, val, 2.88)
    $("#carLoanPayment").text(formatAmountforLoan(repayAmount));
    // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
    ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買車貸_拖拉貸款金額或年限', 'true');
  })
  .build();

//重設試算文字
var sliderAmount = $("#" + carLoanId).slider("value") + 15;

var sliderYear = $("#" + heavyLoanYearForCarID).slider("value");
var loanYear = 18 + (sliderYear * 6);
$("#carLoanYear").text(formatYearforLoan(loanYear));


}

//重機新車 initBar
function initNewHeavyBar() {
  var heavyLoanValueArray = getNumberArray(15, 1, 100);
  var heavyLoanID = "cdHeavySliderAmount";
  var sliderBarForMotoLoans = sliderBuilderForLoan(heavyLoanID)
    .set("valueArray", heavyLoanValueArray)
    .set("defaultValue", 15)
    .event("onSlide", function (val, index) {
      $("#heavyMotoAmount").text(formatAmountforLoan(val));
      var month = parseInt($("#heavyMotoYear").text());
      var repayAmount = handingFeeCalculation(val * 10000, month, 5);
      $("#heavyLoanPayment").text(formatAmountforLoan(repayAmount));
      // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
      ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買重機貸_拖拉貸款金額或年限', 'true');

    })
    .build();

  var heavyLoanYearArray = getNumberArray(18, 6, 60);
  var heavyLoanYearID = "cdHeavySliderYear";
  var sliderBarYearForMotoLoans = sliderBuilderForLoan(heavyLoanYearID)
    .set("valueArray", heavyLoanYearArray)
    .set("defaultValue", 1)
    .event("onSlide", function (val, index) {
      $("#heavyMotoYear").text(formatYearforLoan(val));
      var amount = parseInt($("#heavyMotoAmount").text());
      var repayAmount = handingFeeCalculation(amount * 10000, val, 5)
      $("#heavyLoanPayment").text(formatAmountforLoan(repayAmount));
      // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
      ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買重機貸_拖拉貸款金額或年限', 'true');
    })
    .build();
}
//重機中古車 initBar
function initUsedHeavyBar() {
  var heavyLoanValueArray = getNumberArray(15, 1, 100);
  var heavyLoanID = "cdHeavySliderAmount";
  var sliderBarForMotoLoans = sliderBuilderForLoan(heavyLoanID)
    .set("valueArray", heavyLoanValueArray)
    .set("defaultValue", 15)
    .event("onSlide", function (val, index) {
      $("#heavyMotoAmount").text(formatAmountforLoan(val));
      var month = parseInt($("#heavyMotoYear").text());
      var repayAmount = handingFeeCalculation(val * 10000, month, 6.5);
      $("#heavyLoanPayment").text(formatAmountforLoan(repayAmount));
      // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
      ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買重機貸_拖拉貸款金額或年限', 'true');
    })
    .build();

  var heavyLoanYearArray = getNumberArray(18, 6, 48);
  var heavyLoanYearID = "cdHeavySliderYear";
  var sliderBarYearForMotoLoans = sliderBuilderForLoan(heavyLoanYearID)
    .set("valueArray", heavyLoanYearArray)
    .set("defaultValue", 1)
    .event("onSlide", function (val, index) {
      $("#heavyMotoYear").text(formatYearforLoan(val));
      var amount = parseInt($("#heavyMotoAmount").text());
      var repayAmount = handingFeeCalculation(amount * 10000, val, 6.5)
      $("#heavyLoanPayment").text(formatAmountforLoan(repayAmount));
      // 2021-03-23 Mantis#15706 GA/請協助新增新官網信貸、車貸GA
      ga_trace_event('V2_產品服務_貸款_車貸', 'V2_產品服務_貸款_車貸_買重機貸_拖拉貸款金額或年限', 'true');
    })
    .build();
}

//計算還款
function handingFeeCalculation(amount, month, rate) {
  var monthRate = rate / 12 / 100;
  //分子
  var numerator = (Math.pow((monthRate + 1), month)) * monthRate;
  //分母
  var denominator = (Math.pow((monthRate + 1), month)) - 1;

  var rePaymentAmount = Math.round((numerator / denominator) * amount);
  return rePaymentAmount;
}




<
/script>




<
script type = "text/javascript"
defer >

  if ('' == 'true') {
    ga_send_registry_data('');
  }


// 	if('' == 'true'){
// 	 	ga_send_user_data('','');	
// 	}
if ('' == 'true') {
  ga_send_user_data_b('');
}
if ('' == 'true') {
  ga_send_user_data_c('');
}






if ('' == 'true') {
  ga_send_parents_b_no('');
}
if ('' == 'true') {
  ga_send_parents_m_no('');
}
if ('' == 'true') {
  ga_send_parents_c_no('');
}





var pageTitle = $('#pageTitle').val();
if ('true' == 'true') {
  ga_trace_page(pageTitle);
}

if ('' == 'true') {
  var utmEventPageTitle = $('#utmEventPageTitle').val();
  if (typeof (utmEventPageTitle) != 'undefined' || utmEventPageTitle != '') {
    ga_trace_event_utm(utmEventPageTitle);
  }
}

<
/script>



<
!--小幫手圖示區塊 Start-- >



<
!--2020 / 10 / 26 新增新版小幫手 Start-- >
  <
  div class = "fixed-btn-helper "
onclick = "openHeplerWindow()" >
  <
  img src = "/TSDIB_RichartWeb/static/revamp/images/btn-helper@2x.png"
alt = "" >
  <
  div class = "helper-shadow" > < /div> < /
  div > <
  div class = "dialog-box " >
  <
  p class = "tmp-word" > < /p> < /
  div > <
  !--小幫手圖示區塊 End-- >


  <
  !--2020 / 10 / 26 新增新版智能客服彈出框 Start-- >
  <
  div class = "helper-window-bg " >
  <
  div class = "helper-window " >
  <
  div class = "helper-window-title" >
  <
  h3 > Richart 小幫手 < /h3> <
button type = "button"
class = "btn-close d-inline-block"
onclick = "closeHeplerWindow()" >
  <
  i aria - hidden = "true"
class = "icons-ic-popup-close i-icon" > < /i> < /
  button > <
  /div> <
iframe src = ""
frameborder = "0"
class = "helper-iframe" > < /iframe> < /
  div > <
  /div> <!--智能客服 end-- >








  <
  !--cookie訊息 Start-- >
  <
  div class = "fixed-bottom fixed-cookie d-none" >
  <
  div class = "container" >
  <
  div class = "desc" >
  <
  div class = "row" >
  <
  div class = "col-sm-12 col-md" >
  <
  div class = "" > 您在台新銀行官方網頁瀏覽時， 台新銀行藉由您的瀏覽器會讀取您Cookies， 以便能為您提供更佳、 更個人化交易與各類服務， 方便您參與個人化的互動活動。 < /div> <
div class = "more-cookie" >
  <
  div class = "" > 另台新銀行在網站上亦設有第三方追蹤碼， 經由您的瀏覽器以讀取您的Cookies， 並作以下用途： < /div> <
ul >
  <
  li > 確定及追蹤由第三方網站網路廣告連結至台新銀行網站的訪客量； < /li> <
li > 追蹤台新銀行網站的使用情況； < /li> <
li > 投放使用者所搜尋產品之促銷資訊或廣告； < /li> < /
  ul > <
  div class = "" > 您如果要瞭解更多關於 cookie 或更改 cookie 設置的信息， 請閱讀台新銀行官網的隱私權保護及著作權聲明。 < /div> < /
  div > <
  a href = "#"
class = "btn-more" > < span class = "align-middle" > 了解更多 < /span><i class="i-icon icons-ic-unfold"></i > < /a>

  <
  /div> <
div class = "col-sm-12 col-md-auto text-center" >
  <
  a href = "#"
class = "btn line-white"
id = "jqBtnCloseCookie" > 我知道了 < /a> < /
  div > <
  /div> < /
  div > <
  /div> < /
  div > <
  !--cookie訊息 End-- >


  <
  !--行銷欄 Start-- >






  <
  input type = "hidden"
id = "closePromotionBar"
value = "" / >
  <
  input type = "hidden"
id = "hasBarContent"
value = "true" / >
  <
  !--2020 - 02 - 14 mantis12050 Tom 調整行銷列css-- >
  <
  div class = "fixed-bottom fixed-marketing d-none" >
  <
  div class = "container-fluid" >
  <
  div class = "row" >
  <
  div class = "col col-txt" >
  <
  div class = "d-flex justify-content-center img-dog align-items-md-center align-items-center" >
  <
  img class = "d-none d-md-block"
src = "/TSDIB_RichartWeb/static/revamp/images/img-dog-notice-web.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/img-dog-notice-web@2x.png 2x"
alt = "" >
  <
  img class = "d-md-none"
src = "/TSDIB_RichartWeb/static/revamp/images/img-dog-notice-mobile.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/img-dog-notice-mobile@2x.png 2x"
alt = "" >
  <
  div class = "txt" >
  <
  !--2020 - 05 - 06 Sprint76 GA #26 Rachel -->



                        <a href= "https://richart.tw/TSDIB_RichartWeb/static/event/marketing/202104n/fx/fx2021.html?utm_source=richart&utm_medium=web&utm_campaign=richart_fx_2021new&utm_content=banner_promote"
target = "_blank" > < span class = "align-middle" > Richart 好利High 外幣解鎖不卡關 < /span>  <
i class = "icons-btn-go i-icon d-none d-md-inline-block"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊PromotionBar內文GO按鈕','Richart 好利High 外幣解鎖不卡關');" > < /i> <
i class = "icons-btn-go-m i-icon d-md-none"
onclick = "ga_trace_event('V2_首頁','V2_首頁_點擊PromotionBar內文GO按鈕','Richart 好利High 外幣解鎖不卡關');" > < /i></a >
  <
  /div> < /
  div > <
  /div> <
div class = "col-md-auto d-md-flex align-items-md-center btn-close" >
  <
  a href = "#"
id = "jqBtnCloseMarketing" > < i class = "i-icon icons-ic-black-cross" > < /i></a >
  <
  /div> < /
  div > <
  /div> < /
  div > <
  !--行銷欄 End-- >
  <
  !--footer-- >






  <
  footer >
  <
  div id = 'btnTop' >
  <
  i class = 'icons-ic-arrow-top i-icon ic-preset' > < /i> <
i class = 'icons-ic-arrow-top-h i-icon ic-hover' > < /i> <
span > TOP < /span> < /
  div > <
  div class = 'footer_content' >
  <
  img class = 'footer-bg d-none d-md-block'
width = '100%'
height = 'auto'
src = "/TSDIB_RichartWeb/static/revamp/images/bg-footer.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/bg-footer@2x.png 2x" / >
  <
  img class = 'footer-bg d-md-none'
width = '100%'
height = 'auto'
src = "/TSDIB_RichartWeb/static/revamp/images/bg-footer-m.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/bg-footer-m@2x.png 2x" / >
  <
  div class = 'bottom' >
  <
  div class = 'container max-w-auto' >
  <
  div class = 'row' >
  <
  div class = 'col-sm-12 col-lg-7 col-md-7' >
  <
  !--2020 - 05 - 06 Sprint76 GA #26 Rachel -->



                        <ul id= 'footerMenu'
class = 'd-md-flex align-items-start f-menu' >
  <
  li >
  <
  div class = 'f-project' >
  <
  span > 認識 Richart < /span> <
i class = 'icons-ic-footer-right i-icon d-md-none' > < /i> < /
  div > <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Introduce"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','關於Richart_Richart是誰');" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>Richart 是誰？</a >
  <
  /li> <
li > < a href = "/TSDIB_RichartWeb/RC02/RC020205"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','關於Richart_最新消息');removeNews()" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>最新消息</a >
  <
  /li> < /
  ul > <
  /li> <
li >
  <
  div class = 'f-project' >
  <
  span > APP 功能 < /span> <
i class = 'icons-ic-footer-right i-icon d-md-none' > < /i> < /
  div > <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Expertise/Management?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','APP功能_聰明管錢');goManage(0)" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>聰明管錢</a >
  <
  /li> <
li > < a href = "/TSDIB_RichartWeb/Expertise/Payment?target=0"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','APP功能_輕鬆收付');goExpertise(0)" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>輕鬆收付</a >
  <
  /li> < /
  ul > <
  /li> <
li >
  <
  div class = 'f-project' >
  <
  span > 教學指南 < /span> <
i class = 'icons-ic-footer-right i-icon d-md-none' > < /i> < /
  div > <
  ul >
  <
  li > < a href = "/TSDIB_RichartWeb/Guide"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','教學指南_手把手教學');" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>手把手教學</a >
  <
  /li> <
li > < a href = "/TSDIB_RichartWeb/School/FAQPage"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','教學指南_常見問題Q&A');clearFAQData()" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>常見問題 Q&A</a >
  <
  /li> < /
  ul > <
  /li> <
li >
  <
  div class = 'f-project' >
  <
  span > 聯絡我們 < /span> <
i class = 'icons-ic-footer-right i-icon d-md-none' > < /i> < /
  div > <
  ul >
  <
  li >
  <
  span > < i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
a class = 'd-none d-md-inline' > Richart 客服： 02 - 87989088 < /a> <
a class = 'd-md-none'
href = 'tel:+886-2-87989088'
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','聯絡我們_Richart客服');" > Richart 客服： 02 - 87989088 < /a> < /
  span > <
  /li> <
li >
  <
  span > < i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
a class = 'd-none d-md-inline' > 信用卡客服： 02 - 26553355 < /a> <
a class = 'd-md-none'
href = 'tel:+886-2-26553355'
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','聯絡我們_信用卡客服');" > 信用卡客服： 02 - 26553355 < /a> < /
  span > <
  /li> <
li > < a href = "/TSDIB_RichartWeb/RC04/RC040200"
onclick = "ga_trace_event('V2_首頁','V2_首頁_footer點擊狀況','聯絡我們_線上留言');" >
  <
  i class = 'icons-ic-arrow-footer-column i-icon d-md-none' > < /i> <
i class = 'icons-f-point' > < /i>線上留言</a >
  <
  /li> < /
  ul > <
  /li> < /
  ul > <
  /div> <
div class = 'col-sm-12 col-lg-2 col-md-1 community' >
  <
  span > 社群 < /span> <
ul class = 'f-community' >
  <
  li >
  <
  a href = 'https://www.instagram.com/hellorichart'
class = 'f-link'
target = '_blank' >
  <
  i class = 'icons-btn-social-instagram i-icon ic-preset' > < /i> <
i class = 'icons-btn-social-instagram-h i-icon ic-hover' > < /i></a >
  <
  /li> <
li > < a href = 'https://www.facebook.com/richart.tw/?ref=ts&fref=ts'
class = 'f-link'
target = '_blank' >
  <
  i class = 'icons-btn-social-facebook i-icon ic-preset' > < /i> <
i class = 'icons-btn-social-facebook-h i-icon ic-hover' > < /i></a >
  <
  /li> <
li > < a href = 'https://www.youtube.com/channel/UCDLo1M-oYmoPFD-KpkGQR4g'
class = 'f-link'
target = '_blank' >
  <
  i class = 'icons-btn-social-youtube i-icon ic-preset' > < /i> <
i class = 'icons-btn-social-youtube-h i-icon ic-hover' > < /i></a >
  <
  /li> <
li > < a href = 'https://line.me/R/ti/p/%40richart'
class = 'f-link'
target = '_blank' >
  <
  i class = 'icons-btn-social-line i-icon ic-preset' > < /i> <
i class = 'icons-btn-social-line-h i-icon ic-hover' > < /i></a >
  <
  /li> < /
  ul > <
  /div> <
div class = 'col-sm-12 col-lg-3 col-md-4' >
  <
  div class = 'd-md-flex align-items-start f-notes' >
  <
  div class = 'f-qr text-right' >
  <
  img class = 'd-none d-md-inline-block'
width = '78'
height = '78'
src = "/TSDIB_RichartWeb/static/revamp/images/qr-code.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/qr-code@2x.png 2x" / > < span class = 'd-md-none' > 貼心、 懂你的理財小幫手！ 立即下載！ < /span> < /
  div > <
  div class = 'f-install' >
  <
  !-- < a href = 'https://apps.apple.com/tw/app/richart/id1079733142'
class = 'f-link f-app'
target = '_blank' > -- >
  <
  a href = 'javascript:footerDownloadLink()'
class = 'f-link f-app' >
  <
  div class = 'd-none d-md-inline-block' >
  <
  img width = '114'
height = '35'
src = "/TSDIB_RichartWeb/static/revamp/images/btn-download-apple-s.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/btn-download-apple-s@2x.png 2x"
class = '' / >
  <
  /div> <
div class = 'd-md-none' >
  <
  img width = '200'
height = '50'
src = "/TSDIB_RichartWeb/static/revamp/images/btn-download-apple-footer-m.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/btn-download-apple-footer-m@2x.png 2x" / >
  <
  /div> < /
  a > <
  !-- < a href = 'https://play.google.com/store/apps/details?id=tw.com.taishinbank.richart&hl=zh_TW'
class = 'f-link f-app'
target = '_blank' > -- >
  <
  a href = 'javascript:footerDownloadLink()'
class = 'f-link f-app' >
  <
  div class = 'd-none d-md-inline-block' >
  <
  img width = '114'
height = '35'
src = "/TSDIB_RichartWeb/static/revamp/images/btn-download-googleplay-s.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/btn-download-googleplay-s@2x.png 2x"
class = '' / >
  <
  /div> <
div class = 'd-md-none' >
  <
  img width = '200'
height = '50'
src = "/TSDIB_RichartWeb/static/revamp/images/btn-download-googleplay-m.png"
srcset = "/TSDIB_RichartWeb/static/revamp/images/btn-download-googleplay-m@2x.png 2x" / >
  <
  /div> < /
  a > <
  /div> < /
  div > <
  div class = 'f-version' >
  <
  div class = 'd-none d-md-block' > Richart 目前僅支援 iOS 9.0(含) 以上和 Android 5.0(含) 以上的版本 < /div> <
div class = 'd-md-none text-center' > 自2019年10月2日起， Richart 僅支援 iOS 9.0(含) < br > 以上和 Android 5.0(含) 以上的版本 <
  /div> < /
  div > <
  /div> < /
  div > <
  /div>

  <
  !--2021 - 05 - 13 mantis #0016416 Hannah 新增無障礙專區入口 (V32) -->



		<!-- 2021-06-25 mantis# 16416 將全域footer 無障礙網銀、 無障礙網路ATM 連結使用另開視窗方式開啟(V32) -- >
  <
  div class = 'footer-copyright des-text-12' >
  <
  div class = "link" >
  <
  a href = "https://ac.taishinbank.com.tw/AccWeb/AW2/index.html"
class = "item"
target = "_blank" > 無障礙網銀 < /a> <
span class = "item-divider" > | < /span> <
a href = "https://ac.taishinbank.com.tw/AccWeb/AC/index.html"
class = "item"
target = "_blank" > 無障礙網路ATM < /a> <
span class = "item-divider" > | < /span> <
a href = "https://richart.tw/TSDIB_RichartWeb/RC02/RC020201?announceNo=16467"
class = "item" > 個資法告知事項 < /a> <
span class = "item-divider" > | < /span> <
a href = "https://richart.tw/TSDIB_RichartWeb/RC02/RC020201?announceNo=16468"
class = "item" > 隱私權保護及著作權聲明 < /a> < /
  div > <
  div class = "" > ©台新國際商業銀行 < span class = 'd-sm-block d-md-inline' > Copyright Taishin International Bank.All Rights Reserved. < /span> < /
  div > <
  /div> < /
  div > <
  /div> < /
  footer >

  <
  !--/footer end -->




  <
  form action = "#"
id = "backForm"
method = "POST" > < /form>

  <
  script src = "/TSDIB_RichartWeb/static/revamp/js/globalValue.js?v=2020110501" > < /script> <!--Ar added-- >

  <
  script src = "/TSDIB_RichartWeb/static/revamp/js/anime.js" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/custom.js?v=2020110501" > < /script>

  <
  script src = "/TSDIB_RichartWeb/static/revamp/js/keyboards.js?v=2020110501" > < /script> <
script src = "/TSDIB_RichartWeb/static/revamp/js/InputFilter.js?v=2020110501" > < /script> <
script type = "text/javascript"
src = "/TSDIB_RichartWeb/static/revamp/js/helperModule.js?v=2020110501" > < /script>

  <
  script type = "text/javascript" >
  //2020-10-30 sprint83 WEB#83 調整layoutnew js位置及新增智能客服
  var helperController;

$(document).ready(function () {
  //9698: beanfun/隱藏官網footer App下載文字及按鈕(V20) 20190515 Mark
  if ($('#beanfunFlag').length > 0) {
    //hide footer
    $('#hiddenTag').hide();
  }

  // 2020-10-27 sprint83 WEB#83 tom 新智能客服初始化
  var helperServerUrl = "https://smartrobot.taishinbank.com.tw/chat/index.php/richart?eservice=richartweb";
  var apiUrl = "/TSDIB_RichartWeb/Helper/getHelperMessageDialogList";
  helperController = helperModule()
    .set("helperServerUrl", helperServerUrl)
    .set("apiUrl", apiUrl)
    .build();

})

var atmVersion = "1,0,0,2";
if (window != window.top) {
  window.top.location.replace(window.location);
}


var web_openaccount_page = window.location.origin + '/TSDIB_RichartWeb/Redirect/OpenAccountLink';

$(function () {

  $('input').attr('autocomplete', 'off');

});

function closePromotionsBar() {
  $.ajax({
    url: '/TSDIB_RichartWeb/PromotionBar/close',
    type: 'POST',
    data: {},
    dataType: 'json',
    success: function (data) {

    },
    error: function (xhr) {

    }
  });

}
//ga:連結點擊紀錄 舊版使用方式,目前不使用但保留function 以免出錯
function writeGA_Event_click(str1, str2, str3) {
  if ('' == 'false') {
    //ga('send', 'event',str1,str2,str3);
  }
}

<
!--ios App Store & Google Play
for footer use-- >
function footerDownloadLink() {
  var utmParas = '';
  //確認ＡＰＰ是否可接收到動態utm網址後即可移除
  /* window.location = "/TSDIB_RichartWeb/Redirect/DownloadAppLink?utm_source=v2_richart&utm_medium=click&utm_campaign=v2_richartweb_202003&utm_content=footer"; */
  window.location = "/TSDIB_RichartWeb/Redirect/DownloadAppLink" + utmParas + "";

}

<
!--ios App Store & Google Play
for latestnews use-- >
function newsDownloadLink() {
  var utmParas = '';
  //確認ＡＰＰ是否可接收到動態utm網址後即可移除
  /* window.location = "/TSDIB_RichartWeb/Redirect/DownloadAppLink?utm_source=v2_richart&utm_medium=click&utm_campaign=v2_richartweb_202003&utm_content=News"; */
  window.location = "/TSDIB_RichartWeb/Redirect/DownloadAppLink" + utmParas + "";

}

function historyBack(Url, otherVerify) {
  if (otherVerify === 'otherVerify') {
    $('<input>').attr({
      type: 'hidden',
      name: 'otherVerify',
      value: 'otherVerify'
    }).appendTo('#backForm');
  }
  $("#backForm").attr('action', "/TSDIB_RichartWeb" + Url);
  $("#backForm").submit();
}

//2020-03-03 Sprint73 IT#146 Sony 因應E2EE模組需要添加全域url
var RESTFUL_URL = {
  E2EE_INIT: "/TSDIB_RichartWeb/E2EService/E2EInit",
}

//2020-11-27 by owen 調整11/23小幫手打包檔修正內容
function openHeplerWindow() {
  $('.helper-window-bg').addClass("open");
}

function closeHeplerWindow() {
  $('.helper-window-bg').removeClass("open");
} <
/script> < /
body >

  <
  /html>