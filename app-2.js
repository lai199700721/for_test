// import mode from './modules/mode';
$(function () {
  CreditBar_init();
  MortgageBar_init();
  CarBar_init();
  // new Vue({
  //     el: '#appLoan',
  //     data: {},
  //     mounted: function () {
  //         mode();

  //         this.$nextTick(() => {

  // var swiper = new Swiper('.swiper-container', {
  //     slidesPerView: 1.16,
  //     centeredSlides: true,
  //     on: {
  //         slideChange: () => {
  //             this.status1 = !this.status1;
  //         },
  //     },
  // });
  $('a.f_gold').on('click', function () {
    $('.lb_wrapper').addClass('show');
    $('body').on('click', '.lb_wrapper a.f_gold , .lb_wrapper a.lb_btn', function () {
      $('.lb_wrapper').removeClass('show');
    });
  });

  $('.loan_btn').on('click', function () {
    $('.loan_btn').addClass('loan_btn--action');
    $('.payment-msg').toggleClass('hide');
    $('.loan_inner').slideToggle();
  });
  //數字千分位方法
  function FormatNumber(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
    return arr[0].replace(re, "$1,") + (arr.length == 2 ? "." + arr[1] : "");
  }


  // credit BAR
  function CreditBar_init() {

    let Credit_amountBar = document.getElementById('Credit_amountBar');
    let Credit_amountInput = document.getElementById('Credit_amountInput');
    noUiSlider.create(Credit_amountBar, {
      start: [50],
      tooltips: true,
      connect: [true, false],
      step: 10,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 10,
        'max': 500
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
            Credit_amountBar.noUiSlider.on('update', function (values, handle, unencoded, tap, positions) {
              Credit_amountInput.value = values[handle];

              var pos = positions[0];
              var tooltip = document.querySelector('.noUi-tooltip');
              var handle = document.querySelector('.noUi-handle');

              if (pos <= 5) {
                tooltip.style.transform = "translate(-8%, 50%)";
                handle.style.right = "-8px";
                console.log(handle.style.right);
              } else if (pos >= 90) {
                tooltip.style.transform = "translate(-92%, 50%)";
                handle.style.right = "-4px";
              } else {
                tooltip.style.transform = "translate(-50%, 50%)";
                handle.style.right = "-6px";
              }
            });
    // ______________________0720_________________________________
    // // Connecting the input field
    // Credit_amountBar.noUiSlider.on('update', function (values, handle) {
    //   Credit_amountInput.value = values[handle];
    //   var elements = document.querySelectorAll('.noUi-tooltip');
    //   if (elements.length) {
    //     elements[0].id = 'Credit_amountBar_tooltip';
    //   }
    //   var Credit_amountBar_tooltip = $('#Credit_amountBar_tooltip').html();
    //   console.log(Credit_amountBar_tooltip);
    //   //拉霸位置判斷式
    //   if (Credit_amountBar_tooltip == 500) {
    //     $('#Credit_amountBar_tooltip').css({
    //       left: -25
    //     });
    //   } else if (Credit_amountBar_tooltip == 10) {
    //     $('#Credit_amountBar_tooltip').css({
    //       left: 25
    //     });
    //   } else {
    //     $('#Credit_amountBar_tooltip').css({
    //       left: 0
    //     });
    //   }
    // });

    Credit_amountInput.addEventListener('change', function () {
      Credit_amountBar.noUiSlider.set(this.value);
    });

    let Credit_yearBar = document.getElementById('Credit_yearBar');
    let Credit_yearInput = document.getElementById('Credit_yearInput');
    noUiSlider.create(Credit_yearBar, {
      start: [7],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 7
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    Credit_yearBar.noUiSlider.on('update', function (values, handle) {
      Credit_yearInput.value = values[handle];
      var elements = document.querySelectorAll('.noUi-tooltip');
      if (elements.length) {
        elements[1].id = 'Credit_yearBar_tooltip';
      }
      var Credit_yearBar_tooltip = $('#Credit_yearBar_tooltip').html();
      console.log(Credit_yearBar_tooltip);
      //拉霸位置判斷式
      if (Credit_yearBar_tooltip == 7) {
        $('#Credit_yearBar_tooltip').css({
          left: -25
        });
      } else if (Credit_yearBar_tooltip == 1) {
        $('#Credit_yearBar_tooltip').css({
          left: 25
        });
      } else {
        $('#Credit_yearBar_tooltip').css({
          left: 0
        });
      }
    });

    Credit_yearInput.addEventListener('change', function () {
      Credit_yearBar.noUiSlider.set(this.value);
    });
    //動態計算總額
    Credit_amountBar.noUiSlider.on('update', function (B, b) {

      Credit_yearBar.noUiSlider.on('update', function (C, c) {
        var B_MONEY = B[b];
        var C_YEAR = C[c];
        var rate = (2.6 * 0.01) / 12;
        var month = C_YEAR * 12;
        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

        $("#CreditPayableAmount").html(FormatNumber(Math.round(A_ALL)));

        console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)

      });
    });
  };

  function MortgageBar_init() {
    let M_amountBar = document.getElementById('M_amountBar');
    let M_amountInput = document.getElementById('M_amountInput');
    noUiSlider.create(M_amountBar, {
      start: [800],
      tooltips: true,
      connect: [true, false],
      step: 10,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 100,
        'max': 3000
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
    // Connecting the input field
    M_amountBar.noUiSlider.on('update', function (values, handle) {
      M_amountInput.value = values[handle];
    });

    M_amountInput.addEventListener('change', function () {
      M_amountBar.noUiSlider.set(this.value);
    });

    let M_yearBar = document.getElementById('M_yearBar');
    let M_yearInput = document.getElementById('M_yearInput');
    noUiSlider.create(M_yearBar, {
      start: [30],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 40
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    M_yearBar.noUiSlider.on('update', function (values, handle) {
      M_yearInput.value = values[handle];
    });

    M_yearInput.addEventListener('change', function () {
      M_yearBar.noUiSlider.set(this.value);
    });
    //動態計算總額
    M_amountBar.noUiSlider.on('update', function (B, b) {

      M_yearBar.noUiSlider.on('update', function (C, c) {
        var B_MONEY = B[b];
        var C_YEAR = C[c];
        var rate = (1.31 * 0.01) / 12;
        var month = C_YEAR * 12;
        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (month)) * (rate)) / (Math.pow((1 + (rate)), (month)) - 1)

        $("#HousePayableAmount").html(FormatNumber(Math.round(A_ALL)));

        // console.log(`B_MONEY:${B_MONEY},C_YEAR:${C_YEAR},rate:${rate},month:${month}`)
      });
    });
  };

  function CarBar_init() {
    let Car_amountBar = document.getElementById('Car_amountBar');
    let Car_amountInput = document.getElementById('Car_amountInput');
    noUiSlider.create(Car_amountBar, {
      start: [100],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 10,
        'max': 300
      },
      pips: {
        mode: 'range',
        density: 100,
      }
    });
    // Connecting the input field
    Car_amountBar.noUiSlider.on('update', function (values, handle) {
      Car_amountInput.value = values[handle];
    });

    Car_amountInput.addEventListener('change', function () {
      Car_amountBar.noUiSlider.set(this.value);
    });

    let Car_yearBar = document.getElementById('Car_yearBar');
    let Car_yearInput = document.getElementById('Car_yearInput');
    noUiSlider.create(Car_yearBar, {
      start: [5],
      tooltips: true,
      connect: [true, false],
      step: 1,
      format: {
        from: Number,
        to: function (value) {
          return (Math.round(value));
        }
      },
      range: {
        'min': 1,
        'max': 5
      },
      pips: {
        mode: 'range',
        density: 100
      }
    });
    Car_yearBar.noUiSlider.on('update', function (values, handle) {
      Car_yearInput.value = values[handle];
    });

    Car_yearInput.addEventListener('change', function () {
      Car_yearBar.noUiSlider.set(this.value);
    });
    //動態計算總額
    Car_amountBar.noUiSlider.on('update', function (B, b) {

      Car_yearBar.noUiSlider.on('update', function (C, c) {
        var B_MONEY = B[b]; //車貸金額
        var C_Month = C[c]; //貸款期間
        var rate = (2.38 * 0.01) / 12; //利率÷12

        var A_ALL = B_MONEY * 10000 * (Math.pow((1 + (rate)), (C_Month * 12)) * (rate)) / (Math.pow((1 + (rate)), (C_Month * 12)) - 1)


        $("#CarPayableAmount").html(FormatNumber(Math.round(A_ALL)));

        console.log(`B_MONEY:${B_MONEY},C_Month:${C_Month},rate:${rate},month:${ A_ALL}`)
      });
    });
  };

});
// },
// });

// });
