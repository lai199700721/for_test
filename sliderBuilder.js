var sliderBuilder = function () {
  var _this = this;
  var set = {
    textArray: [],
    valueArray: [],
    defaultValue: "",
  };
  var event = {
    onSlide: function (val, index) {},
  }

  var initSlider = function () {

    $(".cd-slider").slider({
      orientation: "horizontal",
      range: "min",
      min: 0,
      max: set.valueArray.length - 1,
      value: set.defaultValue ? set.defaultValue : 0,
      animate: true,
      slide: function (eve, ui) {
        var index = ui.value;
        event.onSlide(set.valueArray[index], index);
      }
    });
  }
  var initText = function () {
    $(".cd-slider+.cd-slider-tip").empty();
    var html = '<div></div>'
    set.textArray.forEach(function (ele, index) {
      var $html = $(html)
      $html.html(ele)
      if (index < set.textArray.length - 1)
        $html.addClass("mr-auto");
      $(".cd-slider+.cd-slider-tip").append($html);
    });

  }
  var buildInterface = {
    set: function (key, val) {
      set[key] = val;
      return buildInterface;
    },
    event: function (key, fn) {
      event[key] = fn;
      return buildInterface;
    },
    build: function () {
      initSlider();
      //                    initText();
      return userInterface;
    }
  }
  var userInterface = {

  }
  return buildInterface
}

var sliderBuilderForLoan = function (sliderId) {
  var _this = this;
  var set = {
    textArray: [],
    valueArray: [],
    defaultValue: "",
  };
  var event = {
    onSlide: function (val, index) {},
  }

  var initSlider = function () {

    $("#" + sliderId).slider({
      orientation: "horizontal",
      range: "min",
      min: 0,
      max: set.valueArray.length - 1,
      value: set.defaultValue ? set.defaultValue : 0,
      animate: true,
      slide: function (eve, ui) {
        var index = ui.value;
        event.onSlide(set.valueArray[index], index);
      }
    });
  }
  var initText = function () {
    $(".cd-slider+.cd-slider-tip").empty();
    var html = '<div></div>'
    set.textArray.forEach(function (ele, index) {
      var $html = $(html)
      $html.html(ele)
      if (index < set.textArray.length - 1)
        $html.addClass("mr-auto");
      $(".cd-slider+.cd-slider-tip").append($html);
    });

  }
  var buildInterface = {
    set: function (key, val) {
      set[key] = val;
      return buildInterface;
    },
    event: function (key, fn) {
      event[key] = fn;
      return buildInterface;
    },
    build: function () {
      initSlider();
      //            initText();
      return userInterface;
    }
  }
  var userInterface = {

  }
  return buildInterface
}

var formatAmount = function (balanceAmt) {
  var balanceAmtInString = Number(balanceAmt).toString();
  var pattern = /(-?\d+)(\d{3})/;
  var balanceIntegerPart = balanceAmtInString.split(".");

  while (pattern.test(balanceIntegerPart[0])) {
    balanceIntegerPart[0] = balanceIntegerPart[0].replace(pattern, "$1,$2");
  }

  if (balanceIntegerPart[1]) {
    balanceAmtInString = balanceIntegerPart[0] + "." + balanceIntegerPart[1];
  } else {
    balanceAmtInString = balanceIntegerPart[0];
  }

  return "$" + balanceAmtInString;
}
var formatAmountforLoan = function (balanceAmt) {
  var balanceAmtInString = Number(balanceAmt).toString();
  var pattern = /(-?\d+)(\d{3})/;
  var balanceIntegerPart = balanceAmtInString.split(".");

  while (pattern.test(balanceIntegerPart[0])) {
    balanceIntegerPart[0] = balanceIntegerPart[0].replace(pattern, "$1,$2");
  }

  if (balanceIntegerPart[1]) {
    balanceAmtInString = balanceIntegerPart[0] + "." + balanceIntegerPart[1];
  } else {
    balanceAmtInString = balanceIntegerPart[0];
  }
  return balanceAmtInString;
}

var formatYearforLoan = function (balanceAmt) {
  var balanceAmtInString = Number(balanceAmt).toString();
  var pattern = /(-?\d+)(\d{3})/;
  var balanceIntegerPart = balanceAmtInString.split(".");

  while (pattern.test(balanceIntegerPart[0])) {
    balanceIntegerPart[0] = balanceIntegerPart[0].replace(pattern, "$1,$2");
  }

  if (balanceIntegerPart[1]) {
    balanceAmtInString = balanceIntegerPart[0] + "." + balanceIntegerPart[1];
  } else {
    balanceAmtInString = balanceIntegerPart[0];
  }

  return balanceAmtInString;
}

var getNumberArray = function (start, space, end) {
  start = Number(start);
  space = Number(space);
  end = Number(end);
  var result = [];
  for (var i = start; i <= end; i += space) {
    result.push(i);
  }
  return result;
}
var getNumberChiWordByArray = function (list) {
  var result = [];

  var getWord = function (num) {
    var word = [{
        word: "萬",
        num: 10000
      },
      {
        word: "千",
        num: 1000
      },
      {
        word: "百",
        num: 100
      },
      {
        word: "十",
        num: 10
      },
    ]

    var temp = num;
    var index = 0,
      result = "";
    while (temp != 0 && word[index]) {
      if (temp >= word[index].num) {
        result += Math.floor(temp / word[index].num) + word[index].word;
        temp = temp % word[index].num;
      }
      index++;
    }
    return result;
  }
  list.forEach(function (num) {
    result.push(getWord(num))
  });
  return result;
}
var clearFAQData = function () {
  sessionStorage.removeItem("FAQPageStatus");
}
var clearFAQSearchData = function () {
  sessionStorage.removeItem("FAQSearchStatus");
}
var pageLoading = function (options) {
  var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 20, // The line thickness
    radius: 42, // The radius of the inner circle
    scale: 0.5, // Scales overall size of the spinner
    corners: 1, // Corner roundness (0..1)
    color: '#000', // #rgb or #rrggbb or array of colors
    opacity: 0.25, // Opacity of the lines
    rotate: 0, // The rotation offset      
    direction: 1, // 1: clockwise, -1: counterclockwise      
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    className: 'spinner', // The CSS class to assign to the spinner
    top: '50%', // Top position relative to parent
    left: '50%', // Left position relative to parent
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    position: 'absolute' // Element positioning
  };
  if (options && typeof options == "object") {
    Object.keys(options).forEach(function (key) {
      if (opts[key])
        opts[key] = options[key]
    })
  }
  var container = $("<div id='loadContainer' class='is-loading'><div>")
  var spinner = new Spinner(opts).spin();
  container.append(spinner.el);
  $.blockUI({
    message: container,
    showOverlay: true,
    css: {
      cursor: 'default',
      backgroundColor: 'transparent',
      border: '0px'
    },
    overlayCSS: {
      opacity: 0.4,
      cursor: 'default'
    }
  });

}
var pageUnLoading = function () {
  $.unblockUI();
};

//2020-09-14 Mantis#13909: 開戶申請流程戶籍電話、通訊電話、電子郵件欄位檢核(V28) add by Tom Mark
var validBlackList = function (selector, cb) {
  var phoneList = ["02-00000000", "03-0000000", "04-00000000", "05-0000000", "06-0000000", "07-0000000", "08-0000000", "09-0000000"]
  var email = ["na@gmail.com", "na@na.com", "a@gmail.com"];
  $(selector).on("blur", function () {
    var val = $(this).val();
    if (phoneList.indexOf(val) != -1 || email.indexOf(val) != -1)
      cb();
  })
}
// 2020-10-14 tom 移植判斷ie js
var isIE = function () {
  return /.*MSIE.*/.test(navigator.userAgent) || /.*Trident.*/.test(navigator.userAgent);
}

// 2020-10-14 tom button disabled樣式修改
var toggleBtnDisabled = function (seletor, disabled) {
  $(seletor).prop("disabled", disabled)
  // // 2020-10-19 Mantis0014112
  // $(seletor).toggleClass("form-line-grey",disabled).toggleClass("disabled",disabled).toggleClass("form-line-green",!disabled)
}